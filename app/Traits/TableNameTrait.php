<?php

namespace App\Traits;

trait TableNameTrait
{
    public static function getTableName()
    {
        return ((new self)->getTable());
    }
}
