<?php

namespace App\Policies;

use App\Models\Course;
use App\Models\Enrollment;
use App\Models\Lesson;
use App\Models\User;
use Illuminate\Auth\Access\Response;
use Illuminate\Auth\Access\HandlesAuthorization;

class LessonPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the model.
     *
     * @param  User  $user
     * @param  Course $course
     * @param  Enrollment|null $enrollment
     * @return mixed
     */
    public function view(User $user, Lesson $lesson, Course $course, ?Enrollment $enrollment = null)
    {
        if ($user->isAdmin()) {
            return true;
        }

        if ($user->isInstructor()) {
            return intval($course->instructor_id) === intval($user->id)
                ? Response::allow()
                : Response::deny('You do not own this course');
        }

        if ($user->isStudent()) {
            if ($course->isNotAvailable()) {
                return Response::deny('Sorry, the course is not available');
            }

            return $lesson->isPreviewLesson($course)
                || ($enrollment && $enrollment->isPurchased())
                    ? Response::allow()
                    : Response::deny('Sorry, you are not allowed to see this content');
        }

        return Response::deny("No authorization policy found");
    }
}
