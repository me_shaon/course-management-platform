<?php

namespace App\Policies;

use App\Models\Course;
use App\Models\Enrollment;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class CoursePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->isAdmin() || $user->isInstructor();
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Course  $course
     * @return mixed
     */
    public function view(User $user, Course $course, ?Enrollment $enrollment = null)
    {
        if ($user->isAdmin()) {
            return true;
        }

        if ($user->isInstructor()) {
            return intval($course->instructor_id) === intval($user->id)
                ? Response::allow()
                : Response::deny('You do not own this course');
        }

        if ($user->isStudent()) {
            if ($course->isNotAvailable()) {
                return Response::deny('Sorry, the course is not available');
            }

            return $enrollment
                ? Response::allow()
                : Response::deny('Sorry, you are not enrolled to the course');
        }

        return Response::deny("No authorization policy found");
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Course  $course
     * @return mixed
     */
    public function viewAllLesson(User $user, Course $course, ?Enrollment $enrollment = null)
    {
        if ($user->isAdmin()) {
            return true;
        }

        if ($user->isInstructor()) {
            return intval($course->instructor_id) === intval($user->id)
                ? Response::allow()
                : Response::deny('You do not own this course');
        }

        if ($user->isStudent()) {
            if ($course->isNotAvailable()) {
                return Response::deny('Sorry, the course is not available');
            }
            
            return ($enrollment && $enrollment->isPurchased())
                ? Response::allow()
                : Response::deny('Sorry, you are not enrolled to the course');
        }

        return Response::deny("No authorization policy found");
    }

    /**
     * Determine whether the user can view his/her on going courses
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function viewOnGoing(User $user)
    {
        return $user->isStudent();
    }

    /**
     * Determine whether the user can view his/her finished courses
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function viewFinished(User $user)
    {
        return $user->isStudent();
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->isInstructor();
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Course  $course
     * @return mixed
     */
    public function update(User $user, Course $course)
    {
        return $user->isAdmin() ||
            ($user->isInstructor() && intval($course->instructor_id) === intval($user->id));
    }

    /**
     * Determine whether the user can create, update chapters.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Course  $course
     * @return mixed
     */
    public function manageChapter(User $user, Course $course)
    {
        return $user->isAdmin() ||
            ($user->isInstructor() && intval($course->instructor_id) === intval($user->id));
    }

    /**
     * Determine whether the user can create, update lessons.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Course  $course
     * @return mixed
     */
    public function manageLesson(User $user, Course $course)
    {
        return $user->isAdmin() ||
            ($user->isInstructor() && intval($course->instructor_id) === intval($user->id));
    }

    public function markFinished(User $user, Course $course, Enrollment $enrollment)
    {
        return ($user->isStudent()
            && $course->isAvailable()
            && $enrollment
            && $enrollment->isPurchased())
                ? Response::allow()
                : Response::deny();
    }
}
