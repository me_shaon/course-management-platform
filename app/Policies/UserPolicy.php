<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    public function manage(User $user)
    {
        return $user->isAdmin();
    }

    public function editProfile(User $user, User $targetUser)
    {
        return $user->isInstructor() && (intval($user->id) === intval($targetUser->id));
    }

    public function changePassword(User $user, User $targetUser)
    {
        return ($user->isAdmin() || $user->isInstructor())
            && (intval($user->id) === intval($targetUser->id));
    }
}
