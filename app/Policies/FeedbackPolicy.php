<?php

namespace App\Policies;

use App\Models\Course;
use App\Models\Enrollment;
use App\Models\Feedback;
use App\Models\User;
use Illuminate\Auth\Access\Response;
use Illuminate\Auth\Access\HandlesAuthorization;

class FeedbackPolicy
{
    use HandlesAuthorization;

    public function create(User $user, Feedback $feedback, Course $course, Enrollment $enrollment)
    {
        return ($user->isStudent()
            && $course->isAvailable()
            && $enrollment
            && $enrollment->isPurchased())
                ? Response::allow()
                : Response::deny();
    }

    public function edit(User $user, Feedback $feedback, Course $course, Enrollment $enrollment)
    {
        return ($user->isStudent()
            && $course->isAvailable()
            && $enrollment
            && $enrollment->isPurchased()
            && (intval($feedback->user_id) === intval($user->id)))
                ? Response::allow()
                : Response::deny();
    }

    public function viewAny(User $user)
    {
        return $user->isAdmin();
    }

    public function viewPending(User $user)
    {
        return $user->isAdmin();
    }

    public function approve(User $user)
    {
        return $user->isAdmin();
    }
}
