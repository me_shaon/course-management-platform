<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class CouponPolicy
{
    use HandlesAuthorization;

    public function manage(User $user)
    {
        return $user->isAdmin();
    }

    public function check(User $user)
    {
        return $user->isStudent();
    }
}
