<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class EnrollmentPolicy
{
    use HandlesAuthorization;

    public function create(User $user)
    {
        return $user->isStudent();
    }

    public function viewAny(User $user)
    {
        return $user->isAdmin();
    }

    public function makePurchase(User $user)
    {
        return $user->isStudent();
    }

    public function approvePurchase(User $user)
    {
        return $user->isAdmin();
    }

    public function denyPurchase(User $user)
    {
        return $user->isAdmin();
    }

    public function viewPending(User $user)
    {
        return $user->isAdmin();
    }
}
