<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Payment extends Model
{
    use SoftDeletes;

    public const TYPE_INCOME = 'income';
    public const TYPE_EXPENSE = 'expense';

    protected $fillable = [
        'enrollment_id',
        'payable',
        'discount',
        'coupon_id',
        'paid',
        'method',
        'type',
        'category',
        'transaction_id',
        'description',
        'reviewed_by',
        'status'
    ];

    public function enrollment()
    {
        return $this->belongsTo(Enrollment::class);
    }

    public function coupon()
    {
        return $this->belongsTo(Coupon::class);
    }

    public function approvedBy()
    {
        return $this->belongsTo(User::class);
    }
}
