<?php

namespace App\Models;

use App\Constants\PaymentCategory;
use App\Constants\PurchaseStatus;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Enrollment extends Model
{
    use SoftDeletes;

    public const PAGINATION_LENGTH = 20;

    protected $fillable = [
        'course_id',
        'user_id',
        'description',
        'purchase_status',
        'is_finished',
        'is_feedback_given',
        'last_viewed_lesson_order'
    ];

    public function course()
    {
        return $this->belongsTo(Course::class);
    }

    public function student()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function payments()
    {
        return $this->hasMany(Payment::class);
    }

    public function latestPurchase()
    {
        return $this->hasOne(Payment::class)
            ->where('type', Payment::TYPE_INCOME)
            ->where('category', PaymentCategory::ENROLLMENT)
            ->latest();
    }

    /**
     * Scope a query to only include courses which are 'approved'.
     *
     * @param  Builder $query
     * @return Builder
     */
    public function scopePurchased(Builder $query)
    {
        return $query->where('purchase_status', PurchaseStatus::PURCHASED);
    }

    public function isNotPurchased(): bool
    {
        return intval($this->purchase_status) === PurchaseStatus::NOT_PURCHASED;
    }

    public function isPurchased(): bool
    {
        return intval($this->purchase_status) === PurchaseStatus::PURCHASED;
    }

    public function isPurchaseApprovalPending(): bool
    {
        return intval($this->purchase_status) === PurchaseStatus::PURCHASE_APPROVAL_PENDING;
    }

    public function isPurchaseApprovalDeclined(): bool
    {
        return intval($this->purchase_status) === PurchaseStatus::PURCHASE_APPROVAL_DECLINED;
    }
}
