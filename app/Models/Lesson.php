<?php

namespace App\Models;

use App\Traits\TableNameTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Lesson extends Model
{
    use SoftDeletes, TableNameTrait;

    protected $fillable = [
        'title',
        'description',
        'order',
        'link',
        'is_free',
        'chapter_id'
    ];

    public function chapter()
    {
        return $this->belongsTo(Chapter::class);
    }

    public function isFree(): bool
    {
        return (bool)$this->is_free;
    }

    public function isPreviewLesson(Course $course)
    {
        if ($course->isAvailable() && ($course->isFree() || $this->isFree())) {
            return true;
        }

        return false;
    }
}
