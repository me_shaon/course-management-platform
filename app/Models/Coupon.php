<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Coupon extends Model
{
    use SoftDeletes;
    
    public const PAGINATION_LENGTH = 20;
    public const TYPE_FLAT = 'flat';
    public const TYPE_PERCENTAGE = 'percentage';

    public const TYPES = [
        self::TYPE_FLAT,
        self::TYPE_PERCENTAGE
    ];

    protected $fillable = [
        'code',
        'type',
        'discount',
        'course_id',
        'user_id',
        'usage_count',
        'usage_limit'
    ];

    public function course()
    {
        return $this->belongsTo(Course::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
