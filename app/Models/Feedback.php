<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Feedback extends Model
{
    use SoftDeletes;

    protected $table = "feedbacks";

    public const PAGINATION_LENGTH = 20;

    public const RATING_VERY_GOOD = 5;
    public const RATING_GOOD = 4;
    public const RATING_MEDIUM = 3;
    public const RATING_BELOW_AVERAGE = 2;
    public const RATING_NOT_GOOD = 1;

    public const RATINGS = [
        self::RATING_VERY_GOOD,
        self::RATING_GOOD,
        self::RATING_MEDIUM,
        self::RATING_BELOW_AVERAGE,
        self::RATING_NOT_GOOD
    ];

    public const RATING_LABELS = [
        self::RATING_VERY_GOOD => 'Awesome',
        self::RATING_GOOD => 'Good',
        self::RATING_MEDIUM => 'Medium',
        self::RATING_BELOW_AVERAGE => 'Below average',
        self::RATING_NOT_GOOD => 'Not Good'
    ];

    protected $fillable = [
        'course_id',
        'user_id',
        'rating',
        'description',
        'is_approved',
        'approved_by'
    ];

    public function course()
    {
        return $this->belongsTo(Course::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
