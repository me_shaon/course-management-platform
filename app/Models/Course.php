<?php

namespace App\Models;

use App\Traits\TableNameTrait;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class Course extends Model implements HasMedia
{
    use SoftDeletes, TableNameTrait, InteractsWithMedia;

    public const TYPE_FREE = 'free';
    public const TYPE_PREMIUM = 'premium';

    public const TYPES = [
        self::TYPE_FREE,
        self::TYPE_PREMIUM
    ];

    public const STATUS_AVAILABLE = 'available';
    public const STATUS_UPCOMING = 'upcoming';
    public const STATUS_UNAVAILABLE = 'unavailable';

    public const STATUSES = [
        self::STATUS_AVAILABLE,
        self::STATUS_UNAVAILABLE,
        self::STATUS_UPCOMING
    ];

    public const STATUS_COLOR = [
        self::STATUS_AVAILABLE => 'success',
        self::STATUS_UPCOMING => 'warning',
        self::STATUS_UNAVAILABLE => 'danger'
    ];

    protected $fillable = [
        'title',
        'slug',
        'type',
        'status',
        'fee',
        'discount',
        'instructor_id',
        'public_description',
        'bn_public_description',
        'private_description',
        'bn_private_description',
        'number_of_lessons',
        'estimated_completion_time'
    ];

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function instructor()
    {
        return $this->belongsTo(User::class);
    }

    public function chapters()
    {
        return $this->hasMany(Chapter::class)->orderBy('order');
    }

    public function feedbacks()
    {
        return $this->hasMany(Feedback::class)->orderBy('rating', 'desc');
    }

    public function enrollments()
    {
        return $this->hasMany(Enrollment::class);
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }

    /**
     * Scope a query to only include courses which are 'free'.
     *
     * @param  Builder $query
     * @return Builder
     */
    public function scopeFreebies(Builder $query)
    {
        return $query->where('type', self::TYPE_FREE);
    }

    /**
     * Scope a query to only include courses which are 'free'.
     *
     * @param  Builder $query
     * @return Builder
     */
    public function scopePremiums(Builder $query)
    {
        return $query->where('type', self::TYPE_PREMIUM);
    }

    /**
     * Scope a query to only include courses which are 'available'.
     *
     * @param  Builder $query
     * @return Builder
     */
    public function scopeAvailables(Builder $query)
    {
        return $query->where('status', self::STATUS_AVAILABLE);
    }

    /**
     * Scope a query to only include courses which are 'upcoming'.
     *
     * @param  Builder $query
     * @return Builder
     */
    public function scopeUpcomings(Builder $query)
    {
        return $query->where('status', self::STATUS_UPCOMING);
    }

    /**
     * Scope a query to only include courses which are 'unavailable'.
     *
     * @param  Builder $query
     * @return Builder
     */
    public function scopeUnavailables(Builder $query)
    {
        return $query->where('status', self::STATUS_UNAVAILABLE);
    }

    /**
     * Scope a query to only include courses which are 'unavailable'.
     *
     * @param  Builder $query
     * @return Builder
     */
    public function scopeExceptUnavailables(Builder $query)
    {
        return $query->where('status', '<>', self::STATUS_UNAVAILABLE);
    }

    public function registerMediaConversions(?Media $meda = null): void
    {
        $this->addMediaConversion('thumb')
              ->width(540)
              ->height(304)
              ->sharpen(10)
              ->keepOriginalImageFormat();
    }

    public function getPublicDescription(): ?string
    {
        if (app()->isLocale('bn') && !empty($this->bn_public_description)) {
            return $this->bn_public_description;
        }

        return $this->public_description;
    }

    public function getPrivateDescription(): ?string
    {
        if (app()->isLocale('bn') && !empty($this->bn_private_description)) {
            return $this->bn_private_description;
        }

        return $this->private_description;
    }

    public function getBannerImagePath(): string
    {
        $placeholderImage = 'assets/common/images/course_placeholder.jpg';

        return $this->getFirstMedia()
            ? $this->getFirstMediaUrl('default', 'thumb')
            : $placeholderImage;
    }

    public function getStatusColor()
    {
        return self::STATUS_COLOR[$this->status] ?? 'primary';
    }

    public function getFee()
    {
        return $this->type === Course::TYPE_FREE ? 'Free' : $this->fee;
    }

    public function getDiscountedFee()
    {
        return $this->type === Course::TYPE_FREE ? 'Free' : ($this->fee - $this->discount);
    }

    public function hasDiscount(): bool
    {
        if ((int)$this->discount > 0) {
            return true;
        }

        return false;
    }

    public function isAvailable(): bool
    {
        return $this->status === self::STATUS_AVAILABLE;
    }

    public function isUnAvailable(): bool
    {
        return $this->status === self::STATUS_UNAVAILABLE;
    }

    public function isNotAvailable(): bool
    {
        return !$this->isAvailable();
    }

    public function isFree(): bool
    {
        return $this->type === self::TYPE_FREE;
    }

    public function getBkashNumber(): string
    {
        return '01921099556';
    }
}
