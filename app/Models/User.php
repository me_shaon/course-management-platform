<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class User extends Authenticatable implements HasMedia
{
    use Notifiable, SoftDeletes, InteractsWithMedia;

    public const PAGINATION_LENGTH = 20;

    public const ROLE_ADMIN = 'admin';
    public const ROLE_INSTRUCTOR = 'instructor';
    public const ROLE_STUDENT = 'student';

    public const ROLES = [
        self::ROLE_ADMIN,
        self::ROLE_INSTRUCTOR,
        self::ROLE_STUDENT
    ];

    public const AUTH_GOOGLE_CLIENT = 'google';

    public const DEFAULT_USER_AVATAR_IMAGE_PATH = 'assets/common/images/user_placeholder.png';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'bn_name',
        'email',
        'password',
        'email_verified_at',
        'designation',
        'bn_designation',
        'description',
        'bn_description',
        'role',
        'image',
        'github_url',
        'linkedin_url',
        'facebook_url',
        'oauth_id',
        'oauth_client'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function enrollments()
    {
        return $this->hasMany(Enrollment::class);
    }

    /**
     * Scope a query to only include users with role 'admin'.
     *
     * @param  Builder $query
     * @return Builder
     */
    public function scopeAdmins(Builder $query)
    {
        return $query->where('role', self::ROLE_ADMIN);
    }

    /**
     * Scope a query to only include users with role 'instructor'.
     *
     * @param  Builder $query
     * @return Builder
     */
    public function scopeInstructors(Builder $query)
    {
        return $query->where('role', self::ROLE_INSTRUCTOR);
    }

    /**
     * Scope a query to only include users with role 'student'.
     *
     * @param  Builder $query
     * @return Builder
     */
    public function scopeStudents(Builder $query)
    {
        return $query->where('role', self::ROLE_STUDENT);
    }

    public function registerMediaConversions(?Media $meda = null): void
    {
        $this->addMediaConversion('thumb')
              ->width(100)
              ->height(100)
              ->sharpen(10)
              ->keepOriginalImageFormat();
    }

    public function getName(): ?string
    {
        if (app()->isLocale('bn') && !empty($this->bn_name)) {
            return $this->bn_name;
        }

        return $this->name;
    }

    public function getDesignation(): ?string
    {
        if (app()->isLocale('bn') && !empty($this->bn_designation)) {
            return $this->bn_designation;
        }

        return $this->designation;
    }

    public function getDescription(): ?string
    {
        if (app()->isLocale('bn') && !empty($this->bn_description)) {
            return $this->bn_description;
        }

        return $this->description;
    }

    public function getProfileImagePath()
    {
        if ($this->isOAuthUser($this)) {
            return $this->image;
        }

        return asset(
            $this->getFirstMedia()
                ? $this->getFirstMediaUrl('default', 'thumb')
                : self::DEFAULT_USER_AVATAR_IMAGE_PATH
        );
    }

    private function isOAuthUser(User $user): bool
    {
        return $user->oauth_client !== null;
    }

    public function isStudent(): bool
    {
        return $this->role === self::ROLE_STUDENT;
    }

    public function isInstructor(): bool
    {
        return $this->role === self::ROLE_INSTRUCTOR;
    }

    public function isAdmin(): bool
    {
        return $this->role === self::ROLE_ADMIN;
    }
}
