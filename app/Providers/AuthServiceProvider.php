<?php

namespace App\Providers;

use App\Extensions\Provider\EloquentExtendedUserProvider;
use App\Models\Coupon;
use App\Models\Course;
use App\Models\Enrollment;
use App\Models\Feedback;
use App\Models\Lesson;
use App\Models\User;
use App\Policies\CouponPolicy;
use App\Policies\CoursePolicy;
use App\Policies\EnrollmentPolicy;
use App\Policies\FeedbackPolicy;
use App\Policies\LessonPolicy;
use App\Policies\UserPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Auth;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Course::class => CoursePolicy::class,
        Coupon::class => CouponPolicy::class,
        Enrollment::class => EnrollmentPolicy::class,
        Lesson::class => LessonPolicy::class,
        User::class => UserPolicy::class,
        Feedback::class => FeedbackPolicy::class
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Auth::provider('eloquent_extended', function ($app, array $config) {
            // Return an instance of Illuminate\Contracts\Auth\UserProvider...

            return new EloquentExtendedUserProvider($app->make('hash'), $config['model']);
        });
    }
}
