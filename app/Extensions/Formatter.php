<?php

namespace App\Extensions;

use App\Models\Course;

class Formatter
{
    public static function addCurrencyString(string $price)
    {
        $currencyString =  strtolower($price) === Course::TYPE_FREE ? '' : ' BDT';

        return $price . $currencyString;
    }

    public static function toHourMinuteString(?int $minutes)
    {
        $hour = intval($minutes / 60);
        $remainingMinute = $minutes % 60;

        $hourMinuteString = $remainingMinute . "m";

        if ($hour > 0) {
            $hourMinuteString = $hour . "h " . $hourMinuteString;
        }

        return $hourMinuteString;
    }
}
