<?php

namespace App\Extensions\Provider;

use App\Models\User;
use Illuminate\Auth\EloquentUserProvider;
use Illuminate\Contracts\Auth\Authenticatable as UserContract;

class EloquentExtendedUserProvider extends EloquentUserProvider
{
    public function validateCredentials(UserContract $user, array $credentials)
    {
        $passwordMatched = parent::validateCredentials($user, $credentials);

        return $passwordMatched && $this->isUserRoleAllowed($user->role);
    }

    protected function isUserRoleAllowed($role)
    {
        return in_array($role, [User::ROLE_ADMIN, User::ROLE_INSTRUCTOR], true);
    }
}
