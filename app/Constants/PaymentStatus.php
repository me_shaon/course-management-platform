<?php

namespace App\Constants;

class PaymentStatus
{
    public const PENDING = 0;
    public const CONFIRMED = 1;
    public const DECLINED = 2;
}
