<?php

namespace App\Constants;

final class PurchaseStatus
{
    public const NOT_PURCHASED = 0;
    public const PURCHASE_APPROVAL_PENDING = 1;
    public const PURCHASED = 2;
    public const PURCHASE_APPROVAL_DECLINED = 3;

    public const LABELS = [
        self::NOT_PURCHASED => 'Not Purchased',
        self::PURCHASE_APPROVAL_PENDING => 'Request Pending',
        self::PURCHASE_APPROVAL_DECLINED => 'Request Declined',
        self::PURCHASED => 'Purchased'
    ];
}
