<?php

namespace App\Constants;

class PaymentMethod
{
    public const BKASH = 'bkash';
    public const ROCKET = 'rocket';
    public const CASH = 'cash';
    public const BANK = 'bank';

    public const LIST = [
        self::BKASH,
        self::ROCKET,
        self::CASH,
        self::BANK
    ];
}
