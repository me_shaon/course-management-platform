<?php

namespace App\Exceptions;

use Symfony\Component\HttpFoundation\Response;

class CourseRestrictedException extends \Exception
{
    public function __construct($message)
    {
        parent::__construct($message);
    }
}
