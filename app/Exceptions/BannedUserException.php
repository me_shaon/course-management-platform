<?php

namespace App\Exceptions;

use Symfony\Component\HttpFoundation\Response;

class BannedUserException extends \Exception
{
    public function __construct($message = "Access restricted, User is banned")
    {
        parent::__construct($message, Response::HTTP_FORBIDDEN);
    }
}
