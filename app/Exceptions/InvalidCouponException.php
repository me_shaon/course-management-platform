<?php

namespace App\Exceptions;

use Symfony\Component\HttpFoundation\Response;

class InvalidCouponException extends \Exception
{
    public function __construct($message = 'Invalid Coupon Code')
    {
        parent::__construct($message);
    }
}
