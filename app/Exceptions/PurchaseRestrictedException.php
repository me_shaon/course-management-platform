<?php

namespace App\Exceptions;

use Symfony\Component\HttpFoundation\Response;

class PurchaseRestrictedException extends \Exception
{
    public function __construct($message)
    {
        parent::__construct($message);
    }
}
