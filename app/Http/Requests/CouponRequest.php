<?php

namespace App\Http\Requests;

use App\Models\Coupon;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class CouponRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code' => [ 'required' ],
            'discount' => [
                'required',
                'numeric',
                'min:0',
                function ($attribute, $value, $fail) {
                    if (request()->request->get('type') === Coupon::TYPE_PERCENTAGE
                        && $value > 100
                    ) {
                        $fail("Discount can not be more than 100%");
                    }
                }
            ],
            'type' => [ 'required', Rule::in(Coupon::TYPES)],
            'course_id' => [ 'present', 'nullable' ],
            'user_id' => [ 'present', 'nullable' ],
            'usage_limit' => [ 'present']
        ];
    }
}
