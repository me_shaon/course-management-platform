<?php

namespace App\Http\Requests;

use App\Models\Course;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class CreateCourseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => [ 'required' ],
            'type' => ['required', Rule::in(Course::TYPES)],
            'status' => ['required', Rule::in(Course::STATUSES)],
            'fee' => [ 'required', 'numeric', 'min:0'],
            'discount' => [ 'required', 'numeric', 'min:0' ],
            'estimated_completion_time' => [ 'present' ],
            'public_description' => [ 'present' ],
            'bn_public_description' => [ 'present' ],
            'private_description' => [ 'present' ],
            'bn_private_description' => [ 'present' ],
            'image' => [ 'required', 'file' ]
        ];
    }
}
