<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'bn_name' => 'present',
            'designation' => 'required',
            'bn_designation' => 'present',
            'description' => 'present',
            'bn_description' => 'present',
            'github_url' => 'sometimes',
            'linkedin_url' => 'sometimes',
            'facebook_url' => 'sometimes',
            'website_url' => 'sometimes',
            'image' => 'sometimes'
        ];
    }
}
