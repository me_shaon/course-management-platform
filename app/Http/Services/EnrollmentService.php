<?php

namespace App\Http\Services;

use App\Constants\PurchaseStatus;
use App\Models\Course;
use App\Models\Enrollment;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;

class EnrollmentService
{
    /** @var Enrollment */
    protected $enrollment;

    /** @var PaymentService */
    protected $paymentService;

    public function __construct(
        PaymentService $paymentService,
        Enrollment $enrollment
    ) {
        $this->paymentService = $paymentService;
        $this->enrollment = $enrollment;
    }

    /**
     * @param int $courseId
     * @param int $userId
     * @param array $relations
     * @return Enrollment|null
     */
    public function getEnrollment(int $courseId, int $userId, array $relations = []): ?Enrollment
    {
        $enrollment = $this->enrollment->newInstance();

        if (!empty($relations)) {
            $enrollment = $enrollment->with($relations);
        }

        return $enrollment->where('course_id', $courseId)
            ->where('user_id', $userId)
            ->first();
    }

    public function getPaginated(): LengthAwarePaginator
    {
        return Enrollment::with(['course', 'student', 'latestPurchase.coupon'])
            ->paginate(Enrollment::PAGINATION_LENGTH);
    }

    public function getPendingEnrollments(): Collection
    {
        return Enrollment::with(['course', 'student', 'latestPurchase.coupon'])
            ->where('purchase_status', PurchaseStatus::PURCHASE_APPROVAL_PENDING)
            ->get();
    }

    public function createEnrollment(Course $course, int $userId): Enrollment
    {
        $enrollment = $this->enrollment->newInstance();

        DB::transaction(function () use ($course, &$enrollment, $userId) {
            $enrollment->fill([
                'course_id' => $course->id,
                'user_id' => $userId,
                'is_finished' => false,
                'last_viewed_lesson_order' => 1,
                'purchase_status' => $course->isFree()
                    ? PurchaseStatus::PURCHASED
                    : PurchaseStatus::NOT_PURCHASED
            ]);
            $enrollment->save();

            $course->increment('total_enrolled');
        }, 5);

        return $enrollment;
    }
}
