<?php

namespace App\Http\Services;

use Exception;
use App\Models\Chapter;
use App\Models\Course;
use App\Models\Lesson;
use App\Models\Enrollment;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class LessonService
{
    public function create(Course $course, Chapter $chapter, $data)
    {
        $data['order'] = $course->number_of_lessons + 1;
        $data['chapter_id'] = $chapter->id;

        if (!isset($data['is_free'])) {
            $data['is_free'] = 0;
        }

        DB::transaction(function () use ($course, $data) {
            Lesson::create($data);
            $course->increment('number_of_lessons');
        });
    }

    public function update(Lesson $lesson, $data)
    {
        if (!isset($data['is_free'])) {
            $data['is_free'] = 0;
        }

        $lesson->update($data);
    }

    public function getLessonsForCourse(Course $course)
    {
        $lessons = [];

        foreach ($course->chapters as $chapter) {
            foreach ($chapter->lessons as $lesson) {
                $lessons[$lesson->order] = $lesson;
            }
        }

        return $lessons;
    }

    public function getWithPrevNext(array $lessons, int $order)
    {
        if (!isset($lessons[$order])) {
            throw new Exception('Lesson not found');
        }

        $selectedLesson = $lessons[$order];
        $selectedLesson->prev = null;
        $selectedLesson->next = null;

        if ($order > 1) {
            $selectedLesson->prev = $lessons[$order - 1];
        }

        if ($order < count($lessons)) {
            $selectedLesson->next = $lessons[$order + 1];
        }

        return $selectedLesson;
    }

    public function updateLastViewed(Enrollment $enrollment = null, int $order)
    {
        /** @var User $loggedInUser */
        $loggedInUser = auth()->user();
        if ($loggedInUser->isStudent()) {
            $enrollment->update([
                'last_viewed_lesson_order' => $order
            ]);
        }
    }

    public function getLatestOrder(Enrollment $enrollment = null, ?int $order = null)
    {
        if ($order) {
            return $order;
        }

        /** @var User $loggedInUser */
        $loggedInUser = auth()->user();
        if ($loggedInUser->isStudent() && !is_null($enrollment)) {
            return $enrollment->last_viewed_lesson_order;
        }

        return 1;
    }
}
