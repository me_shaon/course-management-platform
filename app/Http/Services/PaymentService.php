<?php

namespace App\Http\Services;

use App\Models\Payment;

class PaymentService
{
    /** @var Payment */
    protected $payment;

    public function __construct(Payment $payment)
    {
        $this->payment = $payment;
    }

    public function createPayment(array $data): Payment
    {
        $payment = $this->payment->newInstance();
        $payment->fill($data);
        $payment->save();

        return $payment;
    }
}
