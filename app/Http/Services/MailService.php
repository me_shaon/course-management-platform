<?php

namespace App\Http\Services;

use Illuminate\Support\Facades\Mail;
use App\Mail\CoursePurchaseApproveEmail;
use App\Mail\CoursePurchaseDeclineEmail;
use App\Models\Enrollment;

class MailService
{
    public function sendPurchaseApprove(Enrollment $enrollment)
    {
        Mail::to($enrollment->student->email)
            ->send(new CoursePurchaseApproveEmail($enrollment->course));
    }

    public function sendPurchaseDecline(Enrollment $enrollment)
    {
        Mail::to($enrollment->student->email)
            ->send(new CoursePurchaseDeclineEmail($enrollment->course));
    }
}
