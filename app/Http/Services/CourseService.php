<?php

namespace App\Http\Services;

use Exception;
use App\Models\Course;
use App\Models\Enrollment;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class CourseService
{
    /** @var Course */
    protected $course;

    public function __construct(Course $course)
    {
        $this->course = $course;
    }

    public function create(array $data): void
    {
        $course = new Course();
        $data['instructor_id'] = auth()->user()->id;
        $this->store($course, $data);
    }

    public function update(Course $course, array $data): void
    {
        // Logic:
        // Course can be edited by 'admin' too.
        // Just assigning 'auth user id' at the time of update will override the actual instructor.
        // So, keeping the original instructor id
        $data['instructor_id'] = $course->instructor_id;
        $this->store($course, $data);
    }

    private function store(Course $course, array $data)
    {
        $data['slug'] = Str::slug($data['title']);

        $course->fill($data);

        DB::transaction(function () use ($course, $data) {
            $course->save();

            if (array_key_exists('image', $data)) {
                $course->addMediaFromRequest('image')->toMediaCollection();
            }
        }, 5);
    }

    public function getOngoingCoursesForStudent(int $userId): Collection
    {
        return Enrollment::with('course')
            ->where('user_id', $userId)
            ->where('is_finished', false)
            ->get()
            ->map(function ($enrollment) {
                return $enrollment->course;
            });
    }

    public function getFinishedCoursesForStudent(int $userId): Collection
    {
        return Enrollment::with('course')
            ->where('user_id', $userId)
            ->where('is_finished', true)
            ->get()
            ->map(function ($enrollment) {
                return $enrollment->course;
            });
    }

    public function getAvailableCourses(): Collection
    {
        return Course::availables()->get();
    }

    public function getAvailableCoursesExcept(array $ids): Collection
    {
        return $this->getAvailableCourses()
            ->reject(function ($course) use ($ids) {
                return in_array($course->id, $ids);
            });
    }

    /**
     * @param string $slug
     * @param array $relations
     * @return Course
     * @throws Exception
     */
    public function getCourseBySlug(string $slug, array $relations = []): Course
    {
        $course = $this->course->newInstance();

        if (!empty($relations)) {
            $course = $course->with($relations);
        }

        $course = $course->where('slug', $slug)->first();

        if (!$course) {
            throw new Exception("Course not found");
        }

        return $course;
    }

    public function getAllForAdmin(): Collection
    {
        return Course::with(['instructor', 'enrollments'])->get();
    }

    public function getAllForInstructor(int $userId): Collection
    {
        return Course::with(['instructor', 'enrollments'])
            ->where('instructor_id', $userId)
            ->get();
    }
}
