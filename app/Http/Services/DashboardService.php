<?php

namespace App\Http\Services;

class DashboardService
{
    private $courseService;

    public function __construct(CourseService $courseService)
    {
        $this->courseService = $courseService;
    }

    public function getDataForStudent(): array
    {
        $onGoingCourses = $this->courseService->getOngoingCoursesForStudent(auth()->user()->id);

        $finishedCourses = $this->courseService->getFinishedCoursesForStudent(auth()->user()->id);

        $onGoingCourseIds = $onGoingCourses->pluck('id')->toArray();

        $finishedCourseIds = $finishedCourses->pluck('id')->toArray();

        $availableCourses = $this->courseService->getAvailableCoursesExcept(
            array_merge($onGoingCourseIds, $finishedCourseIds)
        );

        return [
            'finishedCourses' => $finishedCourses,
            'onGoingCourses' => $onGoingCourses,
            'availableCourses' => $availableCourses
        ];
    }
}
