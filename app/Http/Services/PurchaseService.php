<?php

namespace App\Http\Services;

use App\Constants\PaymentCategory;
use App\Constants\PaymentMethod;
use App\Constants\PaymentStatus;
use App\Constants\PurchaseStatus;
use App\Models\Course;
use App\Models\Enrollment;
use App\Models\Payment;
use Illuminate\Support\Facades\DB;

class PurchaseService
{
    /** @var PaymentService */
    protected $paymentService;

    /** @var CouponService $couponService */
    protected $couponService;

    public function __construct(
        PaymentService $paymentService,
        CouponService $couponService
    ) {
        $this->paymentService = $paymentService;
        $this->couponService = $couponService;
    }

    public function store(Course $course, Enrollment $enrollment, array $data): void
    {
        $coupon = null;

        if ($data['coupon']) {
            $coupon = $this->couponService->getByCode(
                $course,
                auth()->user(),
                $data['coupon']
            );
        }

        DB::transaction(function () use ($enrollment, $course, $data, $coupon) {
            $enrollment->update([
                'purchase_status' => PurchaseStatus::PURCHASE_APPROVAL_PENDING
            ]);

            $payable = $course->getFee();
            $paid = $coupon
                ? $this->couponService->getDiscountedPrice($course, $coupon)
                : $course->getDiscountedFee();
            $discount = $payable - $paid;
            
            $this->paymentService->createPayment([
                'payable' => $payable,
                'paid' => $paid,
                'discount' => $discount,
                'coupon_id' => $coupon ? $coupon->id : null,
                'method' => PaymentMethod::BKASH,
                'type' => Payment::TYPE_INCOME,
                'category' => PaymentCategory::ENROLLMENT,
                'transaction_id' => $data['transaction_id'],
                'enrollment_id' => $enrollment->id,
                'description' => $data['phone_number']
            ]);

            if ($coupon) {
                $coupon->increment('usage_count');
            }
        }, 5);
    }

    public function approve(Enrollment $enrollment)
    {
        $enrollment->purchase_status = PurchaseStatus::PURCHASED;
        $enrollment->latestPurchase->reviewed_by = auth()->user()->id;
        $enrollment->latestPurchase->status = PaymentStatus::CONFIRMED;
        $enrollment->push();
    }

    public function deny(Enrollment $enrollment)
    {
        $enrollment->purchase_status = PurchaseStatus::PURCHASE_APPROVAL_DECLINED;
        $enrollment->latestPurchase->reviewed_by = auth()->user()->id;
        $enrollment->latestPurchase->status = PaymentStatus::DECLINED;
        $enrollment->push();
    }
}
