<?php

namespace App\Http\Services;

use App\Exceptions\InvalidCouponException;
use App\Models\Coupon;
use App\Models\Course;
use App\Models\User;
use Exception;

class CouponService
{
    /**
     * @param Course $course
     * @param User $user
     * @param string $couponCode
     *
     * @return Coupon
     * @throws Exception
     */
    public function getByCode(Course $course, User $user, string $couponCode): Coupon
    {
        $coupon = Coupon::where('code', $couponCode)->first();

        if (!$coupon) {
            throw new InvalidCouponException();
        }

        if (intval($coupon->course_id) !== intval($course->id) && !is_null($coupon->course_id)) {
            throw new InvalidCouponException();
        }

        if (intval($coupon->user_id) !== intval($user->id) && !is_null($coupon->user_id)) {
            throw new InvalidCouponException();
        }

        if (!is_null($coupon->usage_limit) && ($coupon->usage_count >= $coupon->usage_limit)) {
            throw new InvalidCouponException();
        }

        return $coupon;
    }

    public function getDiscountedPrice(Course $course, Coupon $coupon): int
    {
        $courseFee = $course->getFee();

        if ($coupon->type === Coupon::TYPE_FLAT) {
            if ($coupon->discount > $courseFee) {
                return 0;
            } else {
                return $courseFee - $coupon->discount;
            }
        }

        if ($coupon->type === Coupon::TYPE_PERCENTAGE) {
            return $courseFee - intval(($courseFee * $coupon->discount)/100);
        }

        return $courseFee;
    }
}
