<?php

namespace App\Http\Controllers\Website;

use App\Models\Course;

class HomeController
{
    public function __invoke()
    {
        $courses = Course::exceptUnavailables()->get();

        return view('website.home')->with('courses', $courses);
    }
}
