<?php

namespace App\Http\Controllers\Website;

use Exception;
use App\Models\Course;
use Illuminate\View\View;
use Illuminate\Contracts\View\Factory;

class CourseController
{
    /**
     * @param Course $course
     * @return Factory|View
     * @throws Exception
     */
    public function __invoke(Course $course)
    {
        if ($course->isUnAvailable()) {
            return redirect('home')
                ->with('error', 'Sorry, the course is not available');
        }

        $course->load(['instructor', 'feedbacks.user', 'chapters.lessons']);
        return view('website.course-details')->with('course', $course);
    }
}
