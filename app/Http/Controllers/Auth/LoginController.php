<?php

namespace App\Http\Controllers\Auth;

use App\Exceptions\BannedUserException;
use App\Http\Controllers\Controller;
use App\Models\User;
use Laravel\Socialite\Contracts\User as SocialiteUser;
use App\Providers\RouteServiceProvider;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Laravel\Socialite\Facades\Socialite;
use Symfony\Component\HttpFoundation\RedirectResponse;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Redirect the user to the Google authentication page.
     *
     * @return RedirectResponse
     */
    public function redirectToProvider()
    {
        return Socialite::driver('google')->redirect();
    }

    /**
     * Obtain the user information from Google.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function handleProviderCallback()
    {
        try {
            $googleUser = Socialite::driver('google')->user();

            $user = $this->findOrCreateUser($googleUser);

            auth()->login($user, true);
        } catch (BannedUserException $e) {
            return redirect('/')->with('error', $e->getMessage());
        } catch (\Exception $e) {
            return redirect('/')->with('error', 'Login Failed. Please try again.');
        }

        return redirect()->to('dashboard');
    }

    /**
     * @param SocialiteUser $socialiteUser
     * @return User
     * @throws BannedUserException
     */
    private function findOrCreateUser(SocialiteUser $socialiteUser)
    {
        $user = User::where('email', $socialiteUser->email)->first();

        if (!$user) {
            $user = User::create([
                'name' => $socialiteUser->getName(),
                'email' => $socialiteUser->getEmail(),
                'email_verified_at' => Carbon::now(),
                'role' => User::ROLE_STUDENT,
                'password' => $socialiteUser->token, //storing token as password to restrict this user to login in admin panel using email and password
                'oauth_client' => User::AUTH_GOOGLE_CLIENT,
                'oauth_id' => $socialiteUser->getId(),
                'image' => $socialiteUser->getAvatar()
            ]);
        }

        if ($user->is_banned) {
            throw new BannedUserException();
        }

        return $user;
    }
}
