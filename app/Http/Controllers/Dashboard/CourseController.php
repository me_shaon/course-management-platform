<?php

namespace App\Http\Controllers\Dashboard;

use Exception;
use App\Models\User;
use App\Models\Course;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateCourseRequest;
use App\Http\Requests\UpdateCourseRequest;
use App\Http\Services\EnrollmentService;
use App\Http\Services\CourseService;
use App\Models\Feedback;
use App\Policies\FeedbackPolicy;
use Illuminate\View\View;

class CourseController extends Controller
{
    /** @var CourseService */
    private $courseService;

    /** @var EnrollmentService */
    private $enrollmentService;

    public function __construct(CourseService $courseService, EnrollmentService $enrollmentService)
    {
        $this->courseService = $courseService;
        $this->enrollmentService = $enrollmentService;
    }

    public function create()
    {
        $this->authorize('create', Course::class);

        return view('dashboard.pages.course.create');
    }

    public function store(CreateCourseRequest $request)
    {
        $this->authorize('create', Course::class);

        $this->courseService->create($request->validated());

        return redirect()->back()->with('success', 'Course created successfully');
    }

    public function edit(Course $course)
    {
        $this->authorize('update', $course);

        return view('dashboard.pages.course.edit', compact('course'));
    }

    public function update(UpdateCourseRequest $request, Course $course)
    {
        $this->authorize('update', $course);

        $this->courseService->update($course, $request->validated());

        return redirect()->back()->with('success', 'Course edited successfully');
    }

    public function index()
    {
        $this->authorize('viewAny', Course::class);

        $courses = [];
        
        /** @var User $loggedInUser */
        $loggedInUser = auth()->user();

        if ($loggedInUser->isAdmin()) {
            $courses = $this->courseService->getAllForAdmin();
        } elseif ($loggedInUser->isInstructor()) {
            $courses = $this->courseService->getAllForInstructor(auth()->user()->id);
        }

        return view('dashboard.pages.course.index', compact('courses'));
    }

    public function show(Course $course)
    {
        $course->load(['instructor', 'chapters.lessons']);
        
        try {
            $enrollment = null;
            $feedback = null;

            /** @var User $loggedInUser */
            $loggedInUser = auth()->user();

            if ($loggedInUser->isStudent()) {
                $enrollment = $this->enrollmentService->getEnrollment(
                    $course->id,
                    auth()->user()->id,
                    ['course']
                );
                $feedback = Feedback::where('user_id', auth()->user()->id)
                    ->where('course_id', $course->id)
                    ->first();
            }

            $this->authorize('view', [$course, $enrollment]);
        } catch (Exception $e) {
            return redirect()->to('dashboard')->with('error', $e->getMessage());
        }

        return view('dashboard.pages.course.home', compact('course', 'enrollment', 'feedback'));
    }

    public function getOnGoings(): View
    {
        $this->authorize('viewOnGoing', Course::class);

        $onGoingCourses = $this->courseService->getOngoingCoursesForStudent(auth()->user()->id);

        return view('dashboard.pages.course.ongoing', compact('onGoingCourses'));
    }

    public function getFinished(): View
    {
        $this->authorize('viewFinished', Course::class);

        $finishedCourses = $this->courseService->getFinishedCoursesForStudent(auth()->user()->id);

        return view('dashboard.pages.course.finished', compact('finishedCourses'));
    }
}
