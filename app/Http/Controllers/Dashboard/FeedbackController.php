<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\FeedbackRequest;
use App\Http\Services\EnrollmentService;
use App\Models\Course;
use App\Models\Feedback;
use Illuminate\Support\Facades\DB;

class FeedbackController extends Controller
{
    /** @var EnrollmentService */
    protected $enrollmentService;

    public function __construct(EnrollmentService $enrollmentService)
    {
        $this->enrollmentService = $enrollmentService;
    }

    public function create(Course $course)
    {
        $enrollment = $this->enrollmentService->getEnrollment(
            $course->id,
            auth()->user()->id
        );

        $this->authorize('create', [new Feedback(), $course, $enrollment]);

        if ($enrollment->is_feedback_given) {
            $feedback = Feedback::where('user_id', auth()->user()->id)
                ->where('course_id', $course->id)
                ->first();

            return redirect()
                ->route('student.course.feedback.edit', [$course, $feedback]);
        }

        return view('dashboard.pages.feedback.create', compact('course'));
    }

    public function store(FeedbackRequest $reqeust, Course $course)
    {
        $enrollment = $this->enrollmentService->getEnrollment(
            $course->id,
            auth()->user()->id
        );

        $feedback = new Feedback();

        $this->authorize('create', [$feedback, $course, $enrollment]);

        if (!$enrollment) {
            return redirect()->route('dashboard')
                ->with('error', 'Sorry, you need to enroll to the course first');
        }

        $data = $reqeust->validated();
        $data['course_id'] = $course->id;
        $data['user_id'] = auth()->user()->id;
        $data['is_approved'] = false;

        $feedback->fill($data);
        
        DB::transaction(function () use ($feedback, $enrollment) {
            $feedback->save();

            $enrollment->update([
                'is_feedback_given' => true
            ]);
        }, 5);

        return redirect()->route('student.course.show', $course)
            ->with('success', 'Thanks for your feedback');
    }

    public function edit(Course $course, Feedback $feedback)
    {
        $enrollment = $this->enrollmentService->getEnrollment(
            $course->id,
            auth()->user()->id
        );

        $this->authorize('edit', [$feedback, $course, $enrollment]);

        return view('dashboard.pages.feedback.edit', compact('course', 'feedback'));
    }

    public function update(
        FeedbackRequest $reqeust,
        Course $course,
        Feedback $feedback
    ) {
        $enrollment = $this->enrollmentService->getEnrollment(
            $course->id,
            auth()->user()->id
        );

        $this->authorize('edit', [$feedback, $course, $enrollment]);

        if (!$enrollment) {
            return redirect()->route('dashboard')
                ->with('error', 'Sorry, you need to enroll to the course first');
        }

        $data = $reqeust->validated();
        $data['is_approved'] = false;

        $feedback->fill($data);
        $feedback->save();

        return redirect()->route('student.course.show', $course)
            ->with('success', 'Feedback is updated successfully');
    }

    public function index()
    {
        $this->authorize('viewAny', Feedback::class);

        $feedbacks = Feedback::with(['user', 'course'])
            ->paginate(Feedback::PAGINATION_LENGTH);

        return view('dashboard.pages.feedback.index', compact('feedbacks'));
    }

    public function getPending()
    {
        $this->authorize('viewPending', Feedback::class);

        $feedbacks = Feedback::with(['user', 'course'])
            ->where('is_approved', false)
            ->paginate(Feedback::PAGINATION_LENGTH);

        return view('dashboard.pages.feedback.pending-approval', compact('feedbacks'));
    }

    public function approve(Feedback $feedback)
    {
        $this->authorize('approve', $feedback);

        $feedback->update([
            'is_approved' => true,
            'approved_by' => auth()->user()->id
        ]);

        return redirect()->route('feedbacks.pending')
            ->with('success', 'Feedback is approved successfully');
    }
}
