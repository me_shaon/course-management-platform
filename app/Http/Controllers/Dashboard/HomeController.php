<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Services\DashboardService;
use App\Models\User;

class HomeController extends Controller
{
    /** @var DashboardService  */
    private $dashboardService;

    public function __construct(DashboardService $dashboardService)
    {
        $this->dashboardService = $dashboardService;
    }

    public function __invoke()
    {
        /** @var User $loggedInUser */
        $loggedInUser = auth()->user();

        if ($loggedInUser->isStudent()) {
            $viewData = $this->dashboardService->getDataForStudent();

            return view('dashboard.pages.home.student', $viewData);
        } elseif ($loggedInUser->isInstructor()) {
            //TODO implement the logic for instructor
            $viewData = [];

            return view('dashboard.pages.home.instructor', $viewData);
        } elseif ($loggedInUser->isAdmin()) {
            //TODO implement the logic for Admin
            $viewData = [];

            return view('dashboard.pages.home.admin', $viewData);
        }

        abort(404, 'Invalid user role');
    }
}
