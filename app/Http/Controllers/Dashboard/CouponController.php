<?php

namespace App\Http\Controllers\Dashboard;

use App\Extensions\Formatter;
use App\Http\Controllers\Controller;
use App\Http\Requests\CouponRequest;
use App\Http\Services\CouponService;
use App\Models\Coupon;
use App\Models\Course;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class CouponController extends Controller
{
    /** @var CouponService $couponService */
    private $couponService;

    public function __construct(CouponService $couponService)
    {
        $this->couponService = $couponService;
    }
    
    public function index()
    {
        $coupons = Coupon::paginate(Coupon::PAGINATION_LENGTH);

        return view('dashboard.pages.coupon.index', compact('coupons'));
    }

    public function create()
    {
        $courses = Course::all();
        $users = User::students()->get();

        return view('dashboard.pages.coupon.create', compact('courses', 'users'));
    }

    public function store(CouponRequest $request)
    {
        Coupon::create($request->validated());

        return redirect()->back()->with('success', 'Coupon created successfully');
    }

    public function edit(Coupon $coupon)
    {
        $courses = Course::all();
        $users = User::students()->get();

        return view('dashboard.pages.coupon.edit', compact('courses', 'users', 'coupon'));
    }

    public function update(CouponRequest $request, Coupon $coupon)
    {
        $coupon->update($request->validated());

        return redirect()->back()->with('success', 'Coupon updated successfully');
    }

    public function destroy(Coupon $coupon)
    {
        $coupon->delete();

        return response()->json([
            'status' => 'Coupon deleted successfully!'
        ]);
    }

    public function check(Request $request, Course $course)
    {
        $this->authorize('check', Coupon::class);

        $couponCode = $request->query('coupon');

        try {
            $coupon = $this->couponService->getByCode(
                $course,
                auth()->user(),
                $couponCode
            );
        } catch (Exception $e) {
            return response()->json([
                'status' => $e->getMessage()
            ], Response::HTTP_NOT_FOUND);
        }

        $discountedPrice = $this->couponService->getDiscountedPrice($course, $coupon);

        if ($discountedPrice === 0) {
            $message = trans('dashboard.purchase.no_transaction_fee');
        } else {
            $message = trans('dashboard.purchase.transaction_instruction', [
                'amount' => Formatter::addCurrencyString($discountedPrice),
                'bkash_number' => $course->getBkashNumber()
            ]);
        }

        return response()->json([
            'status' => 'success',
            'coupon' => $couponCode,
            'message' => $message
        ]);
    }
}
