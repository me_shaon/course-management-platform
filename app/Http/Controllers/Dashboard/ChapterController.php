<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\ChapterRequest;
use App\Models\Chapter;
use App\Models\Course;

class ChapterController extends Controller
{
    public function index(Course $course)
    {
        $this->authorize('manageChapter', $course);

        $chapters = Chapter::with(['lessons'])
            ->where('course_id', $course->id)
            ->orderBy('order', 'asc')
            ->get();

        return view('dashboard.pages.chapter.index', compact('course', 'chapters'));
    }

    public function store(ChapterRequest $request, Course $course)
    {
        $this->authorize('manageChapter', $course);

        Chapter::create($request->validated());

        return redirect()->back()->with('success', 'Chapter created successfully');
    }

    public function update(ChapterRequest $request, Course $course, Chapter $chapter)
    {
        $this->authorize('manageChapter', $course);
        
        $chapter->update($request->validated());

        return redirect()->back()->with('success', 'Chapter edited successfully');
    }
}
