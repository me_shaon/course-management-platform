<?php

namespace App\Http\Controllers\Dashboard;

use Exception;
use App\Exceptions\CourseRestrictedException;
use App\Exceptions\InvalidCouponException;
use App\Exceptions\PurchaseRestrictedException;
use App\Http\Controllers\Controller;
use App\Http\Services\EnrollmentService;
use Illuminate\View\View;
use App\Http\Requests\PurchaseCourseRequest;
use App\Http\Services\MailService;
use App\Http\Services\PurchaseService;
use App\Models\Course;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Illuminate\Validation\ValidationException;
use App\Models\Enrollment;

class PurchaseController extends Controller
{
    /** @var EnrollmentService  */
    private $enrollmentService;

    /** @var PurchaseService  */
    private $purchaseService;

    /** @var MailService */
    private $mailService;

    public function __construct(
        EnrollmentService $enrollmentService,
        PurchaseService $purchaseService,
        MailService $mailService
    ) {
        $this->enrollmentService = $enrollmentService;
        $this->purchaseService = $purchaseService;
        $this->mailService = $mailService;
    }

    /**
     * @param Course $course
     * @return Factory|View
     * @throws Exception
     */
    public function create(Course $course)
    {
        $this->authorize('makePurchase', Enrollment::class);

        try {
            $enrollment = $this->enrollmentService->getEnrollment($course->id, auth()->user()->id);

            $this->checkPurchaseAbility($course, $enrollment);
        } catch (CourseRestrictedException $e) {
            return redirect()->route('dashboard')
                ->with('error', $e->getMessage());
        } catch (PurchaseRestrictedException $e) {
            return redirect()->route('student.course.show', $course)
                ->with('error', $e->getMessage());
        } catch (Exception $e) {
            return redirect()->back()
                ->with('error', $e->getMessage());
        }

        return view('dashboard.pages.course.purchase', compact('course'));
    }

    /**
     * @param PurchaseCourseRequest $request
     * @param Course $course
     * @return RedirectResponse|Redirector
     * @throws ValidationException
     * @throws Exception
     */
    public function store(PurchaseCourseRequest $request, Course $course)
    {
        $this->authorize('makePurchase', Enrollment::class);

        try {
            $enrollment = $this->enrollmentService->getEnrollment($course->id, auth()->user()->id);

            $this->checkPurchaseAbility($course, $enrollment);

            $this->purchaseService->store($course, $enrollment, $request->validated());
        } catch (CourseRestrictedException $e) {
            return redirect()->route('dashboard')
                ->with('error', $e->getMessage());
        } catch (PurchaseRestrictedException $e) {
            return redirect()->route('student.course.show', $course)
                ->with('error', $e->getMessage());
        } catch (InvalidCouponException $e) {
            return redirect()->back()
                ->with('error', $e->getMessage());
        } catch (Exception $e) {
            return redirect()->back()
                ->withErrors('Sorry, your request can not be processed. Please, try again.');
        }

        return redirect()->route('student.course.purchase.success', $course);
    }

    public function getSuccess(Course $course): View
    {
        return view('dashboard.pages.course.purchase-success', compact('course'));
    }

    public function approve(int $id)
    {
        $this->authorize('approvePurchase', Enrollment::class);

        $enrollment = Enrollment::with(['student', 'course', 'latestPurchase'])
            ->findOrFail($id);

        $this->purchaseService->approve($enrollment);

        try {
            $this->mailService->sendPurchaseApprove($enrollment);
        } catch (Exception $e) {
            return redirect()->back()->with('error', 'Mail Sending failed');
        }
        
        return redirect()->back()->with('success', 'Enrollment purchase Approved successfully');
    }

    public function deny(int $id)
    {
        $this->authorize('denyPurchase', Enrollment::class);

        $enrollment = Enrollment::with('latestPurchase')->findOrFail($id);

        $this->purchaseService->deny($enrollment);

        try {
            $this->mailService->sendPurchaseDecline($enrollment);
        } catch (Exception $e) {
            return redirect()->back()->with('error', 'Mail Sending failed');
        }

        return redirect()->back()->with('success', 'Enrollment purchase Denied successfully');
    }

    private function checkPurchaseAbility(Course $course, Enrollment $enrollment)
    {
        if ($course->isNotAvailable()) {
            throw new CourseRestrictedException('Sorry the course is not available');
        }

        if ($course->isFree()) {
            throw new PurchaseRestrictedException('No need to purchase, the course is totally FREE');
        }

        if (!$enrollment) {
            throw new CourseRestrictedException('You need to first enroll to the course');
        }

        if ($enrollment->isPurchased()) {
            throw new PurchaseRestrictedException('You have already purchased the course');
        }

        if ($enrollment->isPurchaseApprovalPending()) {
            throw new PurchaseRestrictedException('You have already submitted your transaction information for this course. We will verify it shortly and give you full access to the course');
        }
    }
}
