<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\UpdatePasswordRequest;
use App\Http\Requests\UpdateProfileRequest;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    public function edit()
    {
        $user = auth()->user();
        $this->authorize('editProfile', $user);

        return view('dashboard.pages.profile.edit', compact('user'));
    }

    public function update(UpdateProfileRequest $request)
    {
        /** @var User $user */
        $user = auth()->user();
        $this->authorize('editProfile', $user);
        
        $data = $request->validated();
        $image = $data['image'] ?? null;

        $user->fill($data);

        DB::transaction(function () use ($user, $image) {
            $user->save();

            if (isset($image)) {
                $user->addMediaFromRequest('image')->toMediaCollection();
            }
        }, 5);

        return redirect()->back()
            ->with('success', 'Profile updated successfully');
    }

    public function editPassword()
    {
        $user = auth()->user();
        $this->authorize('changePassword', $user);

        return view('dashboard.pages.profile.password', compact('user'));
    }

    public function updatePassword(UpdatePasswordRequest $request)
    {
        /** @var User $user */
        $user = auth()->user();
        $this->authorize('changePassword', $user);

        $password = Hash::make($request->get('password'));
        $user->update([
            'password' => $password
        ]);

        return redirect()->back()
            ->with('success', 'Password updated successfully');
    }
}
