<?php

namespace App\Http\Controllers\Dashboard;

use Exception;
use App\Http\Controllers\Controller;
use App\Http\Services\MailService;
use App\Models\Enrollment;

class MailController extends Controller
{
    /** @var MailService */
    private $mailService;

    public function __construct(MailService $mailService)
    {
        $this->mailService = $mailService;
    }

    public function sendPurchaseApprove(Enrollment $enrollment)
    {
        try {
            $this->mailService->sendPurchaseApprove($enrollment);
        } catch (Exception $e) {
            return redirect()->back()->with('error', 'Mail Sending failed');
        }
        
        return redirect()->back()->with('success', 'Mail sending successful');
    }

    public function sendPurchaseDecline(Enrollment $enrollment)
    {
        try {
            $this->mailService->sendPurchaseDecline($enrollment);
        } catch (Exception $e) {
            return redirect()->back()->with('error', 'Mail Sending failed');
        }
        
        return redirect()->back()->with('success', 'Mail sending successful');
    }
}
