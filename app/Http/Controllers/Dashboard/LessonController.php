<?php

namespace App\Http\Controllers\Dashboard;

use Exception;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateLessonRequest;
use App\Http\Requests\UpdateLessonRequest;
use App\Http\Services\EnrollmentService;
use App\Http\Services\CourseService;
use App\Http\Services\LessonService;
use App\Models\Chapter;
use App\Models\Course;
use App\Models\Lesson;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;
use Symfony\Component\HttpFoundation\Response;

class LessonController extends Controller
{
    /** @var CourseService */
    private $courseService;

    /** @var LessonService */
    private $lessonService;

    /** @var EnrollmentService */
    private $enrollmentService;

    public function __construct(
        CourseService $courseService,
        LessonService $lessonService,
        EnrollmentService $enrollmentService
    ) {
        $this->courseService = $courseService;
        $this->lessonService = $lessonService;
        $this->enrollmentService = $enrollmentService;
    }

    public function create(Course $course, Chapter $chapter)
    {
        $this->authorize('manageLesson', $course);

        return view('dashboard.pages.lesson.create', compact('course', 'chapter'));
    }

    public function store(CreateLessonRequest $request, Course $course, Chapter $chapter)
    {
        $this->lessonService->create($course, $chapter, $request->validated());

        return redirect()->route('chapters.index', [$course, $chapter])
            ->with('success', 'Lesson created successfully');
    }

    public function edit(Course $course, Chapter $chapter, Lesson $lesson)
    {
        $this->authorize('manageLesson', $course);

        return view('dashboard.pages.lesson.edit', compact('course', 'chapter', 'lesson'));
    }

    public function update(
        UpdateLessonRequest $request,
        Course $course,
        Chapter $chapter,
        Lesson $lesson
    ) {
        $this->lessonService->update($lesson, $request->validated());

        return redirect()->route('chapters.index', [$course, $chapter])
            ->with('success', 'Lesson updated successfully');
    }

    /**
     * @param Course $course
     * @param int|null $order
     * @return Factory|View
     * @throws Exception
     */
    public function show(Course $course, ?int $order = null)
    {
        $course->load(['chapters.lessons']);

        $enrollment = $this->enrollmentService->getEnrollment(
            $course->id,
            auth()->user()->id,
            ['course']
        );

        $order = $this->lessonService->getLatestOrder($enrollment, $order);

        $lessons = $this->lessonService->getLessonsForCourse($course);

        try {
            $lesson = $this->lessonService->getWithPrevNext($lessons, $order);
        } catch (Exception $e) {
            abort(Response::HTTP_NOT_FOUND);
        }

        $this->authorize('view', [$lesson, $course, $enrollment]);

        $this->lessonService->updateLastViewed($enrollment, $order);

        return view('dashboard.pages.lesson.show', compact(
            'course',
            'enrollment',
            'lesson'
        ));
    }
}
