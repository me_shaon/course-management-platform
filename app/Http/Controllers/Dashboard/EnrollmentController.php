<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Services\EnrollmentService;
use Illuminate\Http\RedirectResponse;
use App\Http\Services\CourseService;
use App\Models\Course;
use App\Models\Enrollment;

class EnrollmentController extends Controller
{
    /** @var EnrollmentService */
    protected $enrollmentService;

    /** @var CourseService  */
    private $courseService;

    public function __construct(
        EnrollmentService $enrollmentService,
        CourseService $courseService
    ) {
        $this->enrollmentService = $enrollmentService;
        $this->courseService = $courseService;
    }

    /**
     * @param Course $course
     * @return RedirectResponse
     * @throws Exception
     */
    public function store(Course $course): RedirectResponse
    {
        $this->authorize('create', Enrollment::class);

        if ($course->isNotAvailable()) {
            return redirect()->back()
                ->with('error', 'Sorry, the course is not available');
        }

        $enrollment = $this->enrollmentService->getEnrollment($course->id, auth()->user()->id);

        if ($enrollment) {
            session()->flash('error', 'You have already enrolled to the course');
        } else {
            $this->enrollmentService->createEnrollment($course, auth()->user()->id);
            
            session()->flash('success', 'Thanks for joining the course');
        }

        return redirect()
            ->route('student.course.show', $course);
    }

    public function index()
    {
        $this->authorize('viewAny', Enrollment::class);

        $enrollments = $this->enrollmentService->getPaginated();

        return view('dashboard.pages.enrollment.index', compact('enrollments'));
    }

    public function getPending()
    {
        $this->authorize('viewPending', Enrollment::class);

        $enrollments = $this->enrollmentService->getPendingEnrollments();

        return view('dashboard.pages.enrollment.pending-approval', compact('enrollments'));
    }

    public function markFinished(Course $course)
    {
        $enrollment = $this->enrollmentService->getEnrollment(
            $course->id,
            auth()->user()->id
        );
        
        $this->authorize('markFinished', [$course, $enrollment]);

        $enrollment->update([
            'is_finished' => true
        ]);
        
        return redirect()->route('student.course.feedback.create', $course);
    }
}
