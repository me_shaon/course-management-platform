<?php

namespace App\Http\Middleware;

use Closure;

class LanguageDetector
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (session()->has('app_locale')
            && array_key_exists(session()->get('app_locale'), config()->get('languages'))
        ) {
            app()->setLocale(session()->get('app_locale'));
        }
        
        return $next($request);
    }
}
