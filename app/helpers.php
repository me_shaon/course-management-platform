<?php

if (! function_exists('getYoutubeVideoLink')) {
    function getYoutubeVideoLink(string $link)
    {
        return 'https://www.youtube.com/embed/' . $link . '&vq=hd720';
    }
}
