<?php

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes([
    'register' => false,
    'verify' => false
]);

Route::get('lang/{locale}', function ($locale) {
    if (array_key_exists($locale, config('languages'))) {
        session()->put('app_locale', $locale);
    }

    return redirect()->back();
})->name('lang.switch');

Route::get('/redirect', 'Auth\LoginController@redirectToProvider')
    ->name('social_login_redirect');
Route::get('/callback', 'Auth\LoginController@handleProviderCallback');

Route::get('/', 'Website\HomeController')->name('home');

Route::get('courses/{course}/details', 'Website\CourseController')
    ->name('course.public.single');

Route::middleware('auth')->group(function () {
    Route::get('dashboard', 'Dashboard\HomeController')->name('dashboard');

    Route::prefix('me')->group(function () {
        Route::get('password/update', 'Dashboard\ProfileController@editPassword')
            ->name('profile.password.edit');

        Route::patch('password/update', 'Dashboard\ProfileController@updatePassword')
            ->name('profile.password.update');

        Route::get('profile/update', 'Dashboard\ProfileController@edit')
            ->name('profile.edit');

        Route::put('profile/update', 'Dashboard\ProfileController@update')
            ->name('profile.update');
    });


    Route::resource('users', 'Dashboard\UserController')
        ->except(['show', 'destroy'])
        ->middleware('can:manage,App\Models\User');

    Route::resource('coupons', 'Dashboard\CouponController')
        ->except(['show'])
        ->middleware('can:manage,App\Models\Coupon');

    Route::prefix('courses/{course}')->group(function () {
        Route::resource('chapters', 'Dashboard\ChapterController')
            ->except(['create', 'edit', 'show']);
        Route::resource('chapters/{chapter}/lessons', 'Dashboard\LessonController')
            ->only(['create', 'edit', 'store', 'update']);
    });

    Route::resource('courses', 'Dashboard\CourseController')
        ->except(['destroy']);

    Route::get('enrollments/pending', 'Dashboard\EnrollmentController@getPending')
        ->name('enrollments.pending');

    Route::post('enrollments/{id}/purchase/approve', 'Dashboard\PurchaseController@approve')
        ->name('enrollments.purchase.approve');

    Route::post('enrollments/{id}/purchase/deny', 'Dashboard\PurchaseController@deny')
        ->name('enrollments.purchase.deny');

    Route::get('enrollments', 'Dashboard\EnrollmentController@index')
        ->name('enrollments.index');

    Route::get('feedbacks/pending', 'Dashboard\FeedbackController@getPending')
        ->name('feedbacks.pending');

    Route::patch('feedbacks/{feedback}/approve', 'Dashboard\FeedbackController@approve')
        ->name('feedbacks.approve');

    Route::get('feedbacks', 'Dashboard\FeedbackController@index')
        ->name('feedbacks.index');

    Route::get('my/courses/ongoing', 'Dashboard\CourseController@getOnGoings')
        ->name('student.courses.ongoing');

    Route::get('my/courses/finished', 'Dashboard\CourseController@getFinished')
        ->name('student.courses.finished');
    
    Route::get('my/courses/{course}', 'Dashboard\CourseController@show')
        ->name('student.course.show');

    Route::prefix('my/courses/{course}')->group(function () {
        Route::post('enroll', 'Dashboard\EnrollmentController@store')
            ->name('student.course.enroll');

        Route::get('lessons/{order?}', 'Dashboard\LessonController@show')
            ->name('student.course.lesson.show');

        Route::get('purchase', 'Dashboard\PurchaseController@create')
            ->name('student.course.purchase.create');

        Route::post('purchase', 'Dashboard\PurchaseController@store')
            ->name('student.course.purchase.store');

        Route::get('purchase-success', 'Dashboard\PurchaseController@getSuccess')
            ->name('student.course.purchase.success');

        Route::get('coupon-check', 'Dashboard\CouponController@check')
            ->name('student.course.coupon.check');
        
        Route::patch('finish', 'Dashboard\EnrollmentController@markFinished')
            ->name('student.course.finish');

        Route::get('feedback', 'Dashboard\FeedbackController@create')
            ->name('student.course.feedback.create');

        Route::post('feedback', 'Dashboard\FeedbackController@store')
            ->name('student.course.feedback.store');

        Route::get('feedback/{feedback}/edit', 'Dashboard\FeedbackController@edit')
            ->name('student.course.feedback.edit');

        Route::put('feedback/{feedback}/update', 'Dashboard\FeedbackController@update')
            ->name('student.course.feedback.update');
    });

    Route::prefix('mail')->group(function () {
        Route::post('enrollment/{enrollment}/approve', 'Dashboard\MailController@sendPurchaseApprove')
            ->name('mail.send.purchase.approve');
        
        Route::post('enrollment/{enrollment}/decline', 'Dashboard\MailController@sendPurchaseDecline')
            ->name('mail.send.purchase.decline');
    });
});

Route::get('storage_link_hidden_url', function () {
    Artisan::call('storage:link');
});
