@extends('errors.main')

@section('content')
<div class="mx-auto col-lg-6 mt-5 text-center">
    <img class="img-fluid" style="max-width: 400px;" src="{{ asset('assets/dashboard/images/illustrations/server_error.svg') }}" alt="">
    
    <div class="mx-auto mt-5 text-center">
        <h4 class="text-danger">{{__('dashboard.errors.server_error_text')}}</h4>
        <a class="btn btn-dark mt-3" href="{{route('dashboard')}}">{{__('dashboard.errors.go_home_button')}}</a>
    </div>
</div>
@endsection