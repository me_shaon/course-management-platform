@extends('emails.main')

@section('content')
<tr>
    <td valign="middle" class="hero bg_white" style="padding: 2em 0 4em 0;">
      <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%">
          <tr>
              <td style="padding: 0 2.5em; text-align: center; padding-bottom: 3em;">
                  <div class="text">
                      <h2>Sorry!</h2>
                  </div>
              </td>
          </tr>
          <tr>
              <td style="text-align: center;">
              <div class="text-author">
                  <h3 class="name">Your purchase request is declined for course:</h3>
                  <h2>{{$course->title}}</h2>
                  <span class="position">
                      We couldn't verify the purchase information you have given. <br>
                      If you think this is a mistake, please contact us to resolve it.
                   </span>
              </div>
              </td>
          </tr>
      </table>
    </td>
</tr><!-- end tr -->
@endsection
