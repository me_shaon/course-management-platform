@extends('website.layouts.main')

@section('meta')
    <meta property="og:url" content="{{url()->full()}}">
    <meta property="og:type" content="website">
    <meta property="og:title" content="{{$course->title}}">
    <meta property="og:site_name" content="Megaminds Learning">
    <meta property="og:description" content="{{__('website.hero_title')}}">
    <meta property="og:image" content="{{asset($course->getBannerImagePath())}}">
@endsection

@section('page_title', 'Course: ' . $course->title)

@section('content')
    <div class="courses_details_banner">
        <div class="container">
            <div class="row">
                <div class="col-xl-6">
                    <div class="course_text" style="margin-top: 30px;">
                        <img class="img-fluid" src="{{asset($course->getBannerImagePath())}}" alt="">
                        
                        @if($course->isAvailable())
                            <div class="d-flex justify-content-between p-2">
                                <div class="hours">
                                    <div class="video">
                                        <div class="single_video">
                                            <i class="fa fa-youtube-play"></i>
                                            <span>{{$course->number_of_lessons}} Videos</span>
                                        </div>
                                        @if($course->estimated_completion_time)
                                            <div class="single_video">
                                                <i class="fa fa-clock-o"></i>
                                                <span>{{App\Extensions\Formatter::toHourMinuteString($course->estimated_completion_time)}}</span>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="price">
                                    @if(!$course->isFree() && $course->hasDiscount())
                                    <del style="font-size: x-large;margin-right:10px;">{{App\Extensions\Formatter::addCurrencyString($course->getFee())}}</del>
                                    @endif
                                    <span class="active">{{App\Extensions\Formatter::addCurrencyString($course->getDiscountedFee())}}</span>
                                </div>
                            </div>
                        @endif

                    </div>
                </div>
                <div class="col-xl-6">
                    <div class="courses_sidebar">
                        <h4 class="text-center text-white">{{__('website.instructor')}}</h4>
                    <div class="author_info">
                        <div class="auhor_header">
                            <div class="thumb">
                                <img class="rounded-circle" src="{{$course->instructor->getProfileImagePath()}}" alt="">
                            </div>
                            <div class="name">
                                <h3>{{$course->instructor->getName()}}</h3>
                                <p>{{$course->instructor->getDesignation()}}</p>
                            </div>
                        </div>
                        <p class="text_info">
                            {!! nl2br(e($course->instructor->getDescription())) !!}
                        </p>
                        <ul class="text-center">
                            @if($course->instructor->github_url)
                            <li>
                                <a href="{{$course->instructor->github_url}}">
                                    <i class="fa fa-github"></i>
                                </a>
                            </li>
                            @endif
                            @if($course->instructor->linkedin_url)
                                <li>
                                    <a href="{{$course->instructor->linkedin_url}}">
                                        <i class="fa fa-linkedin"></i>
                                    </a>
                                </li>
                            @endif
                            @if($course->instructor->facebook_url)
                                <li>
                                    <a href="{{$course->instructor->facebook_url}}">
                                        <i class="fa fa-facebook"></i>
                                    </a>
                                </li>
                            @endif
                        </ul>
                    </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-5">
            @if($course->isAvailable())
                <div class="col-xl-12 text-center">
                    @if(auth()->user())
                    <a href="{{route('dashboard')}}" class="boxed_btn">{{__('website.go_to_dashboard')}}</a>
                    @else
                    <a href="{{route('login', ['as' => 'student'])}}" class="boxed_btn">{{__('website.enroll_button_text')}}</a>
                    @endif
                </div>
            @else
                <div class="col-xl-12 text-center">
                    <h3 class="text-white">{{__('website.not_available')}}</h3>
                </div>
            @endif
        </div>
    </div>

    <div class="courses_details_info">
        <div class="container">
            <div class="row">
                <div class="col-xl-8 col-lg-8 offset-2">
                    <div class="single_courses">
                        <h3>{{$course->title}}</h3>
                        <p>{!! nl2br(e($course->getPublicDescription())) !!}</p>
                        <h3 class="second_title">{{__('website.course_outline')}}</h3>
                    </div>
                    <div class="outline_courses_info">
                        <div id="accordion">
                            @foreach($course->chapters as $chapter)
                            <div class="card">
                                <div class="card-header">
                                    <h5 class="mb-0">
                                        <i class="fa fa-book"></i>
                                        {{$chapter->title}}
                                    </h5>
                                </div>
                                <div>
                                    <div class="card-body">
                                        <ul class="unordered-list">
                                            @foreach($chapter->lessons as $lesson)
                                                <li>{{$lesson->title}}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>

            @unless($course->feedbacks->isEmpty())
            <hr>
            <div class="row">
                <div class="section-top-border col-xl-8 col-lg-8 offset-2">
                    <h3 class="mb-30">{{__('website.feedback_title')}}</h3>
                    @foreach ($course->feedbacks as $feedback)
                        <div class="row">
                            <div class="col-lg-12">
                                <div>
                                    <h5>{{$feedback->user->name}}</h5>
                            
                                    @include('common.feedback_stars')
                                </div>
                                <blockquote class="generic-blockquote">
                                    {{$feedback->description}}
                                </blockquote>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            @endunless
        </div>
    </div>
@endsection
