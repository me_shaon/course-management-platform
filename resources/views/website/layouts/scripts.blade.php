<script src="{{asset('assets/website/js/vendor/modernizr-3.5.0.min.js')}}"></script>
<script src="{{asset('assets/website/js/vendor/jquery-1.12.4.min.js')}}"></script>
<script src="{{asset('assets/website/js/popper.min.js')}}"></script>
<script src="{{asset('assets/website/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/website/js/waypoints.min.js')}}"></script>
<script src="{{asset('assets/website/js/imagesloaded.pkgd.min.js')}}"></script>
<script src="{{asset('assets/website/js/scrollIt.js')}}"></script>
<script src="{{asset('assets/website/js/jquery.scrollUp.min.js')}}"></script>
<script src="{{asset('assets/website/js/wow.min.js')}}"></script>
<script src="{{asset('assets/website/js/jquery.slicknav.min.js')}}"></script>
<script src="{{asset('assets/website/js/jquery.magnific-popup.min.js')}}"></script>

<script src="{{asset('assets/website/js/main.js')}}"></script>
