<header>
    <div class="header-area ">
        <div id="sticky-header" class="main-header-area">
            <div class="container-fluid p-0">
                <div class="row align-items-center no-gutters">
                    <div class="col-xl-2 col-lg-2">
                        <div class="logo-img">
                            <a href="{{url('/')}}">
                                <img src="{{asset('assets/common/images/logo_with_white_text.png')}}" alt="">
                            </a>
                        </div>
                    </div>
                    <div class="col-xl-9 col-lg-9">
                        <div class="main-menu d-none d-lg-block">
                            <nav>
                                <ul id="navigation">
                                    <li><a class="active" href="{{url('/')}}">{{__('website.home')}}</a></li>
                                    <li><a href="{{ url('/') }}#our_courses">{{__('website.our_courses')}}</a></li>
                                    <li><a href="#contact">{{__('website.contact')}}</a></li>
                                    <li style="margin-left: 100px;">
                                        <a class="" href="#"> {{config()->get('languages')[app()->getLocale()]}} <i class="ti-angle-down"></i></a>
                                        <ul class="submenu">
                                            @foreach (config()->get('languages') as $lang => $language)
                                                @if($lang !== app()->getLocale())
                                                    <li><a href="{{route('lang.switch', $lang)}}">{{$language}}</a></li>
                                                @endif
                                            @endforeach
                                        </ul>
                                    </li>
                                    <li class="pull-right">
                                        @if (auth()->user())
                                            <a class="" href="#">
                                                <img src="{{ auth()->user()->getProfileImagePath() }}" class="menu-user-avatar">
                                                
                                                {{ auth()->user()->name }}
                                                <i class="ti-angle-down"></i>
                                            </a>
                                            <ul class="submenu">
                                                <li><a href="{{route('dashboard')}}">{{__('website.my_dashboard')}}</a></li>
                                                <li>
                                                    <form action="{{route('logout')}}" method="post">
                                                        @csrf
                                                        <button class="genric-btn default-border" type="submit">{{__('website.log_out')}}</button>
                                                    </form>
                                                </li>
                                            </ul>
                                        @else
                                            <a class="" href="#"><i class="flaticon-user"></i> {{__('website.log_in')}} <i class="ti-angle-down"></i></a>
                                            <ul class="submenu">
                                                <li><a href="{{route('login', ['as' => 'student'])}}">{{__('website.as_student')}}</a></li>
                                                <li><a href="{{route('login', ['as' => 'instructor'])}}">{{__('website.as_instructor')}}</a></li>
                                            </ul>
                                        @endif
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="mobile_menu d-block d-lg-none"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
