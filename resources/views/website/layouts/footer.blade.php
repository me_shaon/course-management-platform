<!-- footer -->
<footer class="footer footer_bg_1" id="contact">
    <div class="footer_top">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 text-center">
                    <h4 class="text-white">learning.megaminds@gmail.com</h4>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-12">
                    <div class="footer_widget">
                        <div class="social_links text-center">
                            <ul>
                                <li>
                                    <a data-toggle="tooltip" data-placement="top" title="Facebook Page" href="https://www.facebook.com/learning.megaminds">
                                        <i class="ti-facebook"></i>
                                    </a>
                                </li>
                                <li>
                                    <a data-toggle="tooltip" data-placement="top" title="Youtube Channel" href="https://www.youtube.com/channel/UCRwjMTbkubJlgU_9-J96kxA">
                                        <i class="fa fa-youtube-play"></i>
                                    </a>
                                </li>
                                <li>
                                    <a data-toggle="tooltip" data-placement="top" title="Company Website" href="https://megaminds.technology">
                                        <i class="fa fa-globe"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="copy-right_text">
        <div class="container">
            <div class="footer_border"></div>
            <div class="row">
                <div class="col-xl-12">
                    <p class="copy_right text-center">
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        Copyright &copy; {{date('Y')}} All rights reserved by <a href="https://megaminds.technology">Megaminds</a> <br> This site is designed with the template <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                    </p>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- footer -->
