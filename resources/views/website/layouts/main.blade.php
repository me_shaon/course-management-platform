<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <title>Megaminds Learning - @yield('page_title')</title>
        <meta name="description" content="">
        
        @yield('meta')

        <link rel="shortcut icon" type="image/x-icon" href="{{asset('favicon.png')}}">

        @include('website.layouts.styles')

    </head>

    <body>
        <!--[if lte IE 9]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
        <![endif]-->
        @include('website.components.messenger_chat')

        @include('website.layouts.header')

        @yield('content')

        @include('website.layouts.footer')

        @include('website.layouts.scripts')

        @yield('scripts')
    </body>
</html>
