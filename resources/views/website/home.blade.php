@extends('website.layouts.main')

@section('page_title', 'Home')

@section('content')
    <!-- slider_area_start -->
    <div class="slider_area">
        <div class="single_slider d-flex align-items-center justify-content-center slider_bg_1">
            <div class="container">
                @if(session()->has('error'))
                <div class="row align-items-center justify-content-center">
                    <div class="alert alert-danger alert-dismissible fade show">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                        </button>
                        <strong>{{ session('error') }}</strong>
                    </div>
                </div>
                @endif
                
                <div class="row align-items-center justify-content-center">
                    <div class="col-xl-6 col-md-6">
                        <div class="illastrator_png">
                            <img src="{{asset('assets/website/img/banner/edu_ilastration.png')}}" alt="">
                        </div>
                    </div>
                    <div class="col-xl-6 col-md-6">
                        <div class="slider_info">
                            <h3>{{ __('website.hero_title') }}</h3>
                            <a href="#our_courses" class="boxed_btn">{{__('website.browse_course_button_text')}}</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- slider_area_end -->

    <!-- popular_courses_start -->
    <div class="popular_courses" id="our_courses">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="section_title text-center">
                        <h3>{{__('website.our_courses')}}</h3>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-12">
                    <div class="course_nav">
                        <nav>
                            <ul class="nav" id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab"
                                       aria-controls="home" aria-selected="true">{{__('website.all_courses')}}</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>

        </div>
        <div class="all_courses">
            <div class="container">
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">

                            <div class="row">
                                @foreach($courses as $course)
                                <div class="col-xl-6 col-lg-6 col-md-6">
                                <div class="single_courses">
                                    <div class="thumb">
                                        <a href="{{route('course.public.single', $course)}}">
                                            <img src="{{asset($course->getBannerImagePath())}}" alt="">
                                        </a>
                                    </div>
                                    <div class="courses_info">
                                        <h3>
                                            <a href="{{route('course.public.single', $course)}}">{{$course->title}}</a>
                                        </h3>
                                        <div class="star_price d-flex justify-content-between">
                                            <div class="star">
                                                <span class="font-weight-bold text-{{$course->getStatusColor()}}">{{ucfirst($course->status)}}</span>
                                            </div>
{{--                                            <div class="price">--}}
{{--                                                <span class="offer">$89.00</span>--}}
{{--                                                <span class="active_price">$49</span>--}}
{{--                                            </div>--}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                                @endforeach
                            </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- popular_courses_end-->
@endsection
