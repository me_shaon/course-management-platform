<form method="POST" action="{{ route('login') }}">
    @csrf
    <div class="popup_box">
        <div class="popup_inner">
            <h3 class="text-center"></h3>
            <div class="row">
                <div class="col-xl-12 col-md-12 text-center">
                    <a href="{{route('social_login_redirect')}}" class="">
                        <img class="google-login-img" src="{{asset('assets/website/img/google_logo.png')}}" alt="">
                    </a>
                </div>
            </div>
        </div>
    </div>
</form>
