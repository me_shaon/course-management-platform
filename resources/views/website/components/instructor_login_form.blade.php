<form method="POST" action="{{ route('login') }}">
    @csrf
    <div class="popup_box">
        <div class="popup_inner">
            <h3 class="text-center">{{__('website.sign_in_text')}}</h3>
            <div class="row">
                <div class="col-xl-12 col-md-12 input-div">
                    <input name="email" class="@error('email') is-invalid @enderror" type="email" value="{{ old('email') }}" placeholder="Enter email" required>

                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="col-xl-12 col-md-12 input-div">
                    <input name="password" class="@error('email') is-invalid @enderror" type="password" placeholder="Password" required>
                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="col-xl-12 col-md-12 checkbox-input input-div">
                    <input class="form-check-input" style="width: auto;" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                    <label class="form-check-label" for="remember">
                        {{ __('Remember Me') }}
                    </label>
                </div>
                <div class="col-xl-12">
                    <button type="submit" class="boxed_btn_orange">Sign in</button>
                </div>
            </div>
            @if (Route::has('password.request'))
            <p class="forgot_pass text-center">
                {{-- <a class="" href="{{route('password.request')}}">
                    {{__('website.forgot_password_text')}}
                </a> --}}
                <a class="" href="">
                    {{__('website.forgot_password_text')}}
                </a>
            </p>
            @endif
        </div>
    </div>
</form>
