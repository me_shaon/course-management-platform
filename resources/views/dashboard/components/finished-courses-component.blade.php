@if($finishedCourses->isNotEmpty())
<div class="row">
    <div class="col-12">
        <h4 class="d-inline">{{__('dashboard.course.your_finished_courses')}}</h4>
        <p></p>
        <div class="row">
            @foreach($finishedCourses as $course)
                <div class="col-md-6 col-lg-4">
                    @include('dashboard.components.finished-course-card', ['course' => $course])
                </div>
            @endforeach
        </div>
    </div>
</div>
@endif
