@if($availableCourses->isNotEmpty())
<div class="row">
    <div class="col-12">
        <h4 class="d-inline">{{__('dashboard.course.available_courses')}}</h4>
        <p></p>
        <div class="row">
            @foreach($availableCourses as $course)
                <div class="col-md-6 col-lg-4">
                    @include('dashboard.components.available-course-card', ['course' => $course])
                </div>
            @endforeach
        </div>
    </div>
</div>
@endif
