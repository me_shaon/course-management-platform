<div class="row page-titles mx-0">
    <div class="col p-md-0 text-center">
        <a class="btn btn-primary pull-left" data-toggle="tooltip" data-placement="top" title="" data-original-title="Go back to Dashboard"
           href="{{route('dashboard')}}">
            <i class="icon-home menu-icon"></i>
        </a>

        @if(auth()->user()->isStudent())
            @if($enrollment->isPurchaseApprovalPending())
                <span class="label label-pill label-warning">
                    {{__('dashboard.purchase.pending_for_approval')}}
                </span>
            @endif

            @if($enrollment->isNotPurchased() || $enrollment->isPurchaseApprovalDeclined())
                <span class="label label-pill label-warning">
                    {{__('dashboard.purchase.you_need_to_purchase')}}
                </span>
                <div class="breadcrumb">
                    <a class="btn btn-danger" href="{{route('student.course.purchase.create', $course)}}">
                        <i class="fa fa-shopping-cart"></i>
                        {{__('dashboard.purchase.buy_this_course')}} ({{__('dashboard.purchase.only')}} {{App\Extensions\Formatter::addCurrencyString($course->getDiscountedFee())}})
                    </a>
                </div>
            @endif
        @endif
    </div>
</div>
