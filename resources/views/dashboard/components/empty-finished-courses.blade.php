<div class="row">
    <div class="col-12 text-center">
        <h4 class="d-inline">{{__('dashboard.course.no_finished')}} <br> <a class="text-success" href="{{ route('dashboard') }}">{{__('dashboard.course.lets_enroll')}}</a> </h4>
        <p></p>
        <div class="row" style="margin-top: 40px;">
            <div class="col-md-6 col-lg-6 offset-3">
                <img class="img-fluid" src="{{ asset('assets/dashboard/images/illustrations/blank.svg') }}" alt="">
            </div>
        </div>
    </div>
</div>
