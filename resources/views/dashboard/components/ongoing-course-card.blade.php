<div class="card">
    @include('dashboard.components.course-card-info')

    <div class="card-footer text-center">
        <a href="{{route('student.course.lesson.show', $course)}}" class="btn mb-1 btn-success">
            {{__('dashboard.course.go_to_class')}} <span class="btn-icon-right"><i class="icon-book-open"></i></span>
        </a>
    </div>
</div>
