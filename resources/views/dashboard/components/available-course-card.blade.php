<div class="card">
    @include('dashboard.components.course-card-info')

    <div class="card-footer">
        <div class="d-flex justify-content-center mb-3">
            <span class="label label-success float-left course-card-price">
                @if(!$course->isFree() && $course->hasDiscount())
                <del>{{App\Extensions\Formatter::addCurrencyString($course->getFee())}}</del>
                @endif
                {{App\Extensions\Formatter::addCurrencyString($course->getDiscountedFee())}}
            </span>
        </div>
        <div class="d-flex justify-content-center">
            <form action="{{route('student.course.enroll', $course)}}" method="POST">
                @csrf
    
                <button type="submit" class="btn mb-1 btn-primary float-right">
                    {{__('dashboard.course.enroll')}} <span class="btn-icon-right"><i class="fa fa-arrow-circle-o-right"></i></span>
                </button>
            </form>
        </div>
    </div>
</div>
