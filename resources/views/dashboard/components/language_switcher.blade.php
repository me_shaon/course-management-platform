<li class="icons dropdown d-none d-md-flex">
    <a href="javascript:void(0)" class="log-user"  data-toggle="dropdown">
        <span>{{config()->get('languages')[app()->getLocale()]}}</span>  <i class="fa fa-angle-down f-s-14" aria-hidden="true"></i>
    </a>
    <div class="drop-down dropdown-language animated fadeIn  dropdown-menu">
        <div class="dropdown-content-body">
            <ul>
                @foreach (config()->get('languages') as $lang => $language)
                    @if($lang !== app()->getLocale())
                        <li><a href="{{route('lang.switch', $lang)}}">{{$language}}</a></li>
                    @endif
                @endforeach
            </ul>
        </div>
    </div>
</li>
