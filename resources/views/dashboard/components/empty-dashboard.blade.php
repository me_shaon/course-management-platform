<div class="row">
    <div class="col-12 text-center">
        <h4 class="d-inline" style="line-height: 30px;">{{__('dashboard.course.empty_dashboard')}}</h4>
        <p></p>
        <div class="row" style="margin-top: 40px;">
            <div class="col-md-6 col-lg-6 offset-3">
                <img class="img-fluid" src="{{ asset('assets/dashboard/images/illustrations/under_construction.svg') }}" alt="">
            </div>
        </div>
    </div>
</div>
