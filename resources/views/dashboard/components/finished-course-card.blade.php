<div class="card">
    @include('dashboard.components.course-card-info')

    <div class="card-footer text-center">
        <a href="{{route('student.course.lesson.show', $course)}}" class="btn mb-1 btn-success">
            {{__('dashboard.course.lets_revisit')}} <span class="btn-icon-right"><i class="icon-book-open"></i></span>
        </a>

        <a href="{{route('student.course.feedback.create', $course)}}" class="btn mb-1 btn-warning">
            {{__('dashboard.course.give_feedback')}}
        </a>
    </div>
</div>
