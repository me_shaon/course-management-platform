<a href="{{ route('course.public.single', $course) }}">
    <img class="img-fluid" src="{{asset($course->getBannerImagePath())}}" alt="">
</a>
<div class="card-body">
    <h5 class="card-title"><a href="{{ route('course.public.single', $course) }}">{{ $course->title }}</a> </h5>
    <p class="card-text d-inline mt-10">
        <img class="rounded-circle" style="width: 50px;" src="{{$course->instructor->getProfileImagePath()}}" alt="">
        <small class="text-muted">{{ $course->instructor->getName() }}</small>
    </p>
</div>
