<div class="footer">
    <div class="copyright">
        <p>Copyright &copy; {{date('Y')}} Developed by <a href="https://megaminds.technology">Megaminds</a></p>
        <p>Theme Designed by <a href="https://themeforest.net/user/quixlab">Quixlab</a></p>
    </div>
</div>
