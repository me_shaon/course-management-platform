<div class="nav-header">
    <div class="brand-logo">
        <a href="{{route('dashboard')}}" class="text-center">
            <span class="brand-title">
                <img style="width: 70px" src="{{asset('assets/common/images/logo_with_white_text.png')}}" alt="">
            </span>
        </a>
    </div>
</div>
