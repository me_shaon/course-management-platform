<script src="{{asset('assets/dashboard/js/plugin-common.min.js')}}"></script>
<script src="{{asset('assets/dashboard/js/custom.min.js')}}"></script>
<script src="{{asset('assets/dashboard/js/settings.js')}}"></script>
<script src="{{asset('assets/dashboard/js/gleek.js')}}"></script>
<script src="https://cdn.plyr.io/3.6.2/plyr.polyfilled.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
