@include('dashboard.layouts.common.logo')

<div class="header">
    <div class="header-content clearfix">

        <div class="nav-control">
            <div class="hamburger">
                <span class="toggle-icon"><i class="icon-menu"></i></span>
            </div>
        </div>
{{--        <div class="header-left">--}}
{{--            <div class="input-group icons">--}}
{{--                <div class="input-group-prepend">--}}
{{--                    <span class="input-group-text bg-transparent border-0 pr-2 pr-sm-3" id="basic-addon1"><i class="icon-magnifier"></i></span>--}}
{{--                </div>--}}
{{--                <input type="search" class="form-control" placeholder="Search Dashboard" aria-label="Search Dashboard">--}}
{{--                <div class="drop-down   d-md-none">--}}
{{--                    <form action="#">--}}
{{--                        <input type="text" class="form-control" placeholder="Search">--}}
{{--                    </form>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
        <div class="header-right">
            <ul class="clearfix">

                {{-- @include('dashboard.components.notifications') --}}

                @include('dashboard.components.language_switcher')

                <li class="icons dropdown">
                    <div class="user-img c-pointer position-relative"   data-toggle="dropdown">
                        <span class="activity active"></span>
                        <img src="{{ auth()->user()->getProfileImagePath() }}" height="40" width="40" alt="">
                    </div>
                    <div class="drop-down dropdown-profile   dropdown-menu">
                        <div class="dropdown-content-body">
                            <ul>
                                <li>
                                    <a href="{{url('/')}}"><i class="icon-home"></i> <span>{{__('dashboard.header.go_to_website')}}</span></a>
                                </li>
                                <hr class="my-2">
                                @can('changePassword', auth()->user())
                                <li>
                                    <a href="{{route('profile.password.edit')}}"><i class="icon-key"></i> <span>{{__('dashboard.header.update_password')}}</span></a>
                                </li>
                                <hr class="my-2">
                                @endcan
                                @can('editProfile', auth()->user())
                                <li>
                                    <a href="{{route('profile.edit')}}"><i class="icon-user"></i> <span>{{__('dashboard.header.update_profile')}}</span></a>
                                </li>
                                <hr class="my-2">
                                @endcan
                                <li>
                                    <form action="{{route('logout')}}" method="POST">
                                        @csrf
                                        <button
                                            style="border: none; background: #fff; cursor: pointer; width: 100%;"
                                            type="submit"><i class="icon-key text-danger"></i> <span class="text-danger">{{__('dashboard.header.log_out')}}</span></button>
                                    </form>
                                </li>
                            </ul>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>
