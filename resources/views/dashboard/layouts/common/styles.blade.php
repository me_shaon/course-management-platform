<!-- Custom Stylesheet -->
<link href="{{asset('assets/dashboard/css/style.css')}}" rel="stylesheet">
<link href="https://cdn.plyr.io/3.6.2/plyr.css" rel="stylesheet">
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
<link href="{{asset('assets/dashboard/css/custom.css')}}" rel="stylesheet">
