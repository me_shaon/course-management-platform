<div class="nk-sidebar">
    <div class="nk-nav-scroll">
        <ul class="metismenu" id="menu">
            <li>
                <a href="{{route('dashboard')}}" aria-expanded="false">
                    <i class="icon-home menu-icon"></i><span class="nav-text">{{__('dashboard.sidebar.dashboard')}}</span>
                </a>
            </li>

            @can('manage', App\Models\User::class)
                <li>
                    <a class="has-arrow" href="javascript:void()" aria-expanded="false">
                        <i class="icon-tag menu-icon"></i><span class="nav-text">{{__('dashboard.sidebar.users')}}</span>
                    </a>
                    <ul aria-expanded="false">
                        <li><a href="{{ route('users.index') }}">{{__('dashboard.sidebar.user_list')}}</a></li>
                        <li><a href="{{ route('users.create') }}">{{__('dashboard.sidebar.create_new_user')}}</a></li>
                    </ul>
                </li>
            @endcan

            @if(auth()->user()->isStudent())
                <li>
                    <a class="has-arrow" href="javascript:void()" aria-expanded="false">
                        <i class="icon-book-open menu-icon"></i><span class="nav-text">{{__('dashboard.sidebar.my_courses')}}</span>
                    </a>
                    <ul aria-expanded="false">
                        <li><a href="{{ route('student.courses.ongoing') }}">{{__('dashboard.sidebar.ongoing_courses')}}</a></li>
                        <li><a href="{{ route('student.courses.finished') }}">{{__('dashboard.sidebar.finished_courses')}}</a></li>
                    </ul>
                </li>
            @endif

            @canany(['viewAny', 'create'], App\Models\Course::class)
                <li>
                    <a class="has-arrow" href="javascript:void()" aria-expanded="false">
                        <i class="icon-book-open menu-icon"></i><span class="nav-text">{{__('dashboard.sidebar.courses')}}</span>
                    </a>
                    <ul aria-expanded="false">
                        @can('viewAny', App\Models\Course::class)
                            <li><a href="{{ route('courses.index') }}">{{__('dashboard.sidebar.course_list')}}</a></li>
                        @endcan
                        
                        @can('create', App\Models\Course::class)
                            <li><a href="{{ route('courses.create') }}">{{__('dashboard.sidebar.create_new_course')}}</a></li>
                        @endcan
                    </ul>
                </li>
            @endcanany

            @can('manage', App\Models\Coupon::class)
                <li>
                    <a class="has-arrow" href="javascript:void()" aria-expanded="false">
                        <i class="icon-tag menu-icon"></i><span class="nav-text">{{__('dashboard.sidebar.coupons')}}</span>
                    </a>
                    <ul aria-expanded="false">
                        <li><a href="{{ route('coupons.index') }}">{{__('dashboard.sidebar.coupon_list')}}</a></li>
                        <li><a href="{{ route('coupons.create') }}">{{__('dashboard.sidebar.create_new_coupon')}}</a></li>
                    </ul>
                </li>
            @endcan

            @canany(['viewAny', 'viewPending'], App\Models\Enrollment::class)
                <li>
                    <a class="has-arrow" href="javascript:void()" aria-expanded="false">
                        <i class="icon-notebook menu-icon"></i><span class="nav-text">{{__('dashboard.sidebar.enrollments')}}</span>
                    </a>
                    <ul aria-expanded="false">
                        @can('viewAny', App\Models\Enrollment::class)
                            <li><a href="{{ route('enrollments.index') }}">{{__('dashboard.sidebar.enrollment_list')}}</a></li>
                        @endcan
                        @can('viewPending', App\Models\Enrollment::class)
                        <li><a href="{{ route('enrollments.pending') }}">{{__('dashboard.sidebar.pending_enrollments')}}</a></li>
                        @endcan
                    </ul>
                </li>
            @endcanany

            @canany(['viewAny', 'viewPending'], App\Models\Feedback::class)
                <li>
                    <a class="has-arrow" href="javascript:void()" aria-expanded="false">
                        <i class="icon-envelope menu-icon"></i><span class="nav-text">{{__('dashboard.sidebar.feedbacks')}}</span>
                    </a>
                    <ul aria-expanded="false">
                        @can('viewAny', App\Models\Feedback::class)
                            <li><a href="{{ route('feedbacks.index') }}">{{__('dashboard.sidebar.feedback_list')}}</a></li>
                        @endcan
                        @can('viewPending', App\Models\Feedback::class)
                        <li><a href="{{ route('feedbacks.pending') }}">{{__('dashboard.sidebar.pending_feedbacks')}}</a></li>
                        @endcan
                    </ul>
                </li>
            @endcanany
        </ul>
    </div>
</div>
