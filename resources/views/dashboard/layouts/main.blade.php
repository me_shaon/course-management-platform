<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <title>Megaminds: @yield('page_title')</title>

        <!-- Favicon icon -->
        <link rel="icon" type="image/png" sizes="16x16" href="{{asset('favicon.png')}}">

        @include('dashboard.layouts.common.styles')
        @yield('styles')
    </head>

    <body>

        @include('dashboard.layouts.common.preloader')

        <div id="main-wrapper">

            @include('dashboard.layouts.common.header')

            @yield('sidebar')

            <div class="content-body">

                @yield('breadcrumb')

                <div class="container-fluid">
                    @include('dashboard.components.flash-message')

                    @yield('content')
                </div>
            </div>

            @include('dashboard.layouts.common.footer')
        </div>

        @include('dashboard.layouts.common.scripts')
        @yield('scripts')
    </body>
</html>
