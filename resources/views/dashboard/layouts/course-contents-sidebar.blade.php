<div class="nk-sidebar">
    <div class="nk-nav-scroll">
        <ul class="metismenu" id="menu">
            <li>
                <a class="font-weight-bold" href="{{route('student.course.show', $course)}}" aria-expanded="false">
                    <i class="icon-notebook menu-icon"></i>
                    <span class="nav-text">{{$course->title}}</span>
                </a>
            </li>
            <li class="nav-label">{{__('dashboard.course.chapters')}}</li>

            @foreach($course->chapters as $chapter)
                <li>
                    <a class="has-arrow" href="javascript:void();" aria-expanded="false">
                        <i class="icon-book-open menu-icon"></i>
                        <span class="nav-text">{{ $chapter->title }}</span>
                    </a>
                    <ul aria-expanded="false">
                        @foreach($chapter->lessons as $lesson)
                            <li>
                                @can('view', [$lesson, $course, $enrollment])
                                <a href="{{route('student.course.lesson.show', [$course, $lesson->order])}}">
                                    {{$lesson->title}}
                                </a>
                                @endcan
                                @cannot('view', [$lesson, $course, $enrollment])
                                    <a href="javascript:void();"
                                       class="disabled"
                                       data-toggle="tooltip"
                                       data-placement="top"
                                        title="" data-original-title="{{__('dashboard.course.buy_to_view')}}">
                                        <i class="icon-lock"></i> {{$lesson->title}}
                                    </a>
                                @endcannot
                            </li>
                        @endforeach
                    </ul>
                </li>
            @endforeach
        </ul>
    </div>
</div>
