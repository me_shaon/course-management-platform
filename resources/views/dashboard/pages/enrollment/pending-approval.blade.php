@extends('dashboard.layouts.main')

@section('page_title', 'Pending Enrollments for Approval')

@section('sidebar')
    @include('dashboard.layouts.sidebar')
@endsection

@section('content')
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">{{__('dashboard.course.pending_enrollments_for_approval')}}</h4>
                <div class="table-responsive">
                    <table class="table header-border">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>User Name</th>
                            <th>User Email</th>
                            <th>Course</th>
                            <th>Transaction ID</th>
                            <th>Transaction Method</th>
                            <th>Transaction Details</th>
                            <th>Coupon</th>
                            <th>Approve</th>
                            <th>Deny</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($enrollments as $enrollment)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$enrollment->student->name}}</td>
                                <td>{{$enrollment->student->email}}</td>
                                <td class="text-primary">{{$enrollment->course->title}}</td>
                                <td class="text-warning">{{$enrollment->latestPurchase->transaction_id}}</td>
                                <td>{{$enrollment->latestPurchase->method}}</td>
                                <td>{{$enrollment->latestPurchase->description}}</td>
                                <td>{{$enrollment->latestPurchase->coupon ? $enrollment->latestPurchase->coupon->code : ''}}</td>
                                <td>
                                    <form action="{{route('enrollments.purchase.approve', ['id' => $enrollment->id])}}" method="POST">
                                        @csrf
                                        <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-check"></i></button>
                                    </form>
                                </td>
                                <td>
                                    <form action="{{route('enrollments.purchase.deny', ['id' => $enrollment->id])}}" method="POST">
                                        @csrf
                                        <button class="btn btn-danger btn-sm"><i class="fa fa-times"></i></button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
