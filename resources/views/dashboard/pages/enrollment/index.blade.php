@extends('dashboard.layouts.main')

@section('page_title', 'Enrollment List')

@section('sidebar')
    @include('dashboard.layouts.sidebar')
@endsection

@section('content')
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">{{__('dashboard.course.enrollment_list')}}</h4>
                <div class="table-responsive">
                    <table class="table header-border">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>User Name</th>
                            <th>User Email</th>
                            <th>Course</th>
                            <th>Transaction ID</th>
                            <th>Transaction Method</th>
                            <th>Transaction Details</th>
                            <th>Coupon</th>
                            <th>Status</th>
                            <th>Send Approve Mail</th>
                            <th>Send Deny Mail</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($enrollments as $enrollment)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$enrollment->student->name}}</td>
                                <td>{{$enrollment->student->email}}</td>
                                <td class="text-primary">{{$enrollment->course->title}}</td>
                                <td class="text-warning">{{optional($enrollment->latestPurchase)->transaction_id}}</td>
                                <td>{{optional($enrollment->latestPurchase)->method}}</td>
                                <td>{{optional($enrollment->latestPurchase)->description}}</td>
                                <td>{{optional($enrollment->latestPurchase)->coupon ? optional($enrollment->latestPurchase)->coupon->code : ''}}</td>
                                <td>{{App\Constants\PurchaseStatus::LABELS[$enrollment->purchase_status]}}</td>
                                <td>
                                    <form action="{{route('mail.send.purchase.approve', $enrollment)}}" method="POST">
                                        @csrf
                                        <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-check"></i></button>
                                    </form>
                                </td>
                                <td>
                                    <form action="{{route('mail.send.purchase.decline', $enrollment)}}" method="POST">
                                        @csrf
                                        <button class="btn btn-danger btn-sm"><i class="fa fa-times"></i></button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{$enrollments->links()}}
                </div>
            </div>
        </div>
    </div>
@endsection
