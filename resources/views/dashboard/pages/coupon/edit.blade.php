@extends('dashboard.layouts.main')

@section('page_title', 'Coupon create')

@section('sidebar')
    @include('dashboard.layouts.sidebar')
@endsection

@section('content')
    <div class="col-lg-12">
        @include('dashboard.components.validation-errors')

        <div class="card">
            <div class="card-header">
                <h4 class="text-center">{{__('dashbaord.course.update_coupon')}}</h4>
            </div>
            <div class="card-body">
                <div class="form-validation">
                    <form class="form-valide" action="{{route('coupons.update', $coupon->id)}}" method="post">
                        @csrf
                        @method('PUT')
                        
                        <div class="form-group row">
                            <label class="col-lg-4 col-form-label">Code <span class="text-danger">*</span>
                            </label>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" name="code" value="{{old('code', $coupon->code)}}" placeholder="Enter a Code.." required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-4 col-form-label">Discount <span class="text-danger">*</span>
                            </label>
                            <div class="col-lg-6">
                                <input type="number" min="0" class="form-control" name="discount" value="{{old('discount', $coupon->discount)}}" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-4 col-form-label">Type <span class="text-danger">*</span>
                            </label>
                            <div class="col-lg-6">
                                <select class="form-control" name="type">
                                    @foreach(\App\Models\Coupon::TYPES as $type)
                                    <option {{ (old('type', $coupon->type) === $type) ? 'selected' : '' }} value="{{$type}}">{{ucfirst($type)}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-4 col-form-label">Assign to Course
                            </label>
                            <div class="col-lg-6">
                                <select class="form-control course-select" name="course_id">
                                    <option value="">All</option>
                                    @foreach($courses as $course)
                                        <option {{ (old('course_id', $coupon->course_id) === $course->id) ? 'selected' : '' }} value="{{$course->id}}">{{$course->title}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-4 col-form-label">Assign to User
                            </label>
                            <div class="col-lg-6">
                                <select class="form-control user-select" name="user_id">
                                    <option value="">All</option>
                                    @foreach($users as $user)
                                        <option {{ (old('user_id', $coupon->user_id) === $user->id) ? 'selected' : '' }} value="{{$user->id}}">{{$user->name}} ({{$user->email}})</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-4 col-form-label">Usage Limit
                            </label>
                            <div class="col-lg-6">
                                <input type="number" min="0" class="form-control" name="usage_limit" value="{{old('usage_limit', $course->usage_limit)}}">
                            </div>
                        </div>

                        <div class="d-flex justify-content-center">
                            <button type="submit" class="btn btn-success">{{__('dashboard.buttons.submit')}}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            $('.course-select').select2();
            $('.user-select').select2();
        });
    </script>
@endsection
