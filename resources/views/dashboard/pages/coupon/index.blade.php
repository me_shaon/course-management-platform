@extends('dashboard.layouts.main')

@section('page_title', 'Coupons List')

@section('sidebar')
    @include('dashboard.layouts.sidebar')
@endsection

@section('content')
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title pull-left">{{__('dashboard.course.coupon_list')}}</h4>
                <a class="pull-right btn btn-info" href="{{ route('coupons.create') }}">Add Coupon</a>
                <div class="table-responsive mt-5">
                    <table class="table table-bordered table-striped verticle-middle">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Code</th>
                            <th scope="col">Discount</th>
                            <th scope="col">Type</th>
                            <th scope="col">For Course</th>
                            <th scope="col">For User</th>
                            <th scope="col">Usage Count</th>
                            <th scope="col">Usage Limit</th>
                            <th scope="col">Edit</th>
                            <th scope="col">Delete</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($coupons as $coupon)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$coupon->code}}</td>
                                <td>{{$coupon->discount}}</td>
                                <td>{{$coupon->type}}</td>
                                <td>{{$coupon->course_id ? $coupon->course->title : 'All'}}</td>
                                <td>{{$coupon->user_id ? $coupon->user->name : 'All'}}</td>
                                <td>{{$coupon->usage_count}}</td>
                                <td>{{$coupon->usage_limit}}</td>
                                <td>
                                    <span><a href="{{ route('coupons.edit', $coupon->id)}}" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"><i class="fa fa-pencil color-muted m-r-5"></i></a></span>
                                </td>
                                <td class="text-center">
                                    <span><a href="{{ route('coupons.destroy', $coupon->id)}}" class="delete-button" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"><i class="fa fa-times text-danger m-r-5"></i></a></span>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{$coupons->links()}}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    @include('dashboard.layouts.common.delete-script')
@endsection