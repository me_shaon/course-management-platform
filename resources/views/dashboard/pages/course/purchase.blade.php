@extends('dashboard.layouts.main')

@section('page_title', 'Course Purchase')

@section('sidebar')
    @include('dashboard.layouts.sidebar')
@endsection

@section('content')
    <div class="row">
        <div class="col-6 offset-3">

            @include('dashboard.layouts.common.errors')
            <div>
                <div class="alert alert-info">
                    {{__('dashboard.purchase.notice_transaction_limit')}}
                </div>
            </div>

            <div class="card">
                <div class="card-body">
                    <div class="card-title text-center">
                        <h3 class="text-danger">{{$course->title}}</h3>
                        <h5 class="text-success">
                            {{__('dashboard.purchase.thanks_for_interest')}}
                        </h5>
                        <hr>
                        <div class="d-flex justify-content-center" id="coupon-apply-button">
                            <button class="btn btn-primary apply-coupon">{{__('dashboard.purchase.apply_coupon_button_text')}}</button>
                        </div>
                        <h6 class="mt-4" style="line-height: 24px;" id="bkash-instruction">
                            {!! __('dashboard.purchase.transaction_instruction', ['amount' => App\Extensions\Formatter::addCurrencyString($course->getDiscountedFee()), 'bkash_number' => $course->getBkashNumber()]) !!}
                        </h6>
                    </div>

                    <form class="mt-4" action="{{route('student.course.purchase.store', $course)}}" method="POST">
                        @csrf

                        <div class="form-group">
                            <input type="text" name="phone_number" class="form-control input-rounded" placeholder="Phone Number" required>
                        </div>
                        <div class="form-group">
                            <input type="text" name="transaction_id" class="form-control input-rounded" placeholder="Transaction ID" required>
                        </div>
                        <div class="form-group d-none" id="coupon-div">
                            <input type="text" id="coupon-field" name="coupon" class="form-control input-rounded" placeholder="Coupon Code" readonly>
                        </div>

                        <div class="form-group">
                            <div class="d-flex justify-content-center">
                                <button type="submit" class="btn btn-success">{{__('dashboard.buttons.submit')}}</button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script>
    $('body').on('click','.apply-coupon', function(e){
        e.preventDefault();

        Swal.fire({
            title: 'Submit your Coupon Code',
            input: 'text',
            inputAttributes: {
                autocapitalize: 'off'
            },
            showCancelButton: true,
            confirmButtonText: 'Apply',
            showLoaderOnConfirm: true,
            preConfirm: (coupon) => {
                var url = '{{route('student.course.coupon.check', $course)}}' + '?coupon=' + coupon;

                return fetch(url, {
                    'method': 'GET',
                    'credentials': "same-origin",
                    headers: {
                        "Content-Type": "application/json",
                        "Accept": "application/json",
                        "X-Requested-With": "XMLHttpRequest",
                        "X-CSRF-Token": "{{csrf_token()}}"
                    }
                })
                .then(response => {
                    if (!response.ok) {
                        if (response.status === 404) {
                            throw new Error("Invalid Coupon Code");
                        }

                        throw new Error(response.statusText);
                    }
                    return response.json()
                })
                .catch(error => {
                    Swal.showValidationMessage(
                        `${error}`
                    )
                })
            },
            allowOutsideClick: () => !Swal.isLoading()
        }).then((result) => {
            console.log(result);
            if (result.isConfirmed && result.value.status === 'success') {
                Swal.fire (
                    'Coupon Code applied successfully!',
                    '',
                    'success'
                );
                $('#bkash-instruction').html(result.value.message);
                $('#coupon-field').val(result.value.coupon);
                $('#coupon-div').removeClass('d-none');
                $('#coupon-apply-button').removeClass('d-flex').addClass('d-none');
            }
        });
    });
</script>
@endsection
