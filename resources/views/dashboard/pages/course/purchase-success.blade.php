@extends('dashboard.layouts.main')

@section('page_title', 'Course Purchase Successful')

@section('sidebar')
    @include('dashboard.layouts.sidebar')
@endsection

@section('content')
    <div class="row">
        <div class="col-12 text-center">
            <div style="line-height: 3;">
                <h3 class="d-inline">
                    <span class="text-success">{{__('dashboard.purchase.success.title')}}</span><br>
                </h3>
                <h4 class="d-inline">
                    {{__('dashboard.purchase.success.body')}}<br>
                    <a href="{{route('student.course.show', $course)}}" class="btn btn-primary">{{__('dashboard.purchase.success.button_text')}}</a>
                </h4>
            </div>
            <p></p>
            <div class="row" style="margin-top: 40px;">
                <div class="col-md-6 col-lg-6 offset-3">
                    <img class="img-fluid" src="{{ asset('assets/dashboard/images/illustrations/congratulations.svg') }}" alt="">
                </div>
            </div>
        </div>
    </div>
@endsection
