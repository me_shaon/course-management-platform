@extends('dashboard.layouts.main')

@section('page_title', 'Ongoing Courses')

@section('sidebar')
    @include('dashboard.layouts.sidebar')
@endsection

@section('content')
    @if($onGoingCourses->isEmpty())
        @include('dashboard.components.empty-ongoing-courses')
    @else
        @include('dashboard.components.ongoing-courses-component')
    @endif
@endsection
