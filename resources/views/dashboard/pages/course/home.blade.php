@extends('dashboard.layouts.main')

@section('page_title', 'Course - ' . $course->title)

@section('sidebar')
    @include('dashboard.layouts.course-contents-sidebar')
@endsection

@section('breadcrumb')
    @include('dashboard.components.course-contents-breadcrumb')
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12 col-sm-12">
            <div class="card gradient-1">
                <div class="card-body text-center">
                    <h3 class="card-title text-white">{{__('dashboard.course.welcome')}}</h3>
                    <div class="d-inline-block">
                        <h2 class="text-white">{{$course->title}}</h2>
                        <p class="text-white mb-0"><strong>{{__('dashboard.course.number_of_lessons')}}:</strong> {{$course->number_of_lessons}}</p>
                        @if($course->estimated_completion_time)
                        <p class="text-white mb-0"><strong>{{__('dashboard.course.completion_time')}}:
                            </strong> {{ App\Extensions\Formatter::toHourMinuteString($course->estimated_completion_time)}}
                        </p>
                        @endif

                        @if ($course->number_of_lessons > 0)
                            <a href="{{route('student.course.lesson.show', $course)}}" class="btn mb-1 btn-success mt-4">
                                {{__('dashboard.course.lets_explore')}} <span class="btn-icon-right"><i class="icon-book-open"></i></span>
                            </a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-8 col-sm-8">
            <div class="card">
                <div class="card-body">
                    <div class="d-inline-block">
                        <h5>{{__('dashboard.course.description')}}</h5>
                        <p>{!! nl2br(e($course->getPublicDescription())) !!}</p>
                    </div>
                </div>
            </div>

            @if(!empty($course->getPrivateDescription()))
            <div class="card">
                <div class="card-body">
                    <div class="d-inline-block">
                        <h5>{{__('dashboard.course.private_note')}}</h5>
                        <p>{!! nl2br(e($course->getPrivateDescription())) !!}</p>
                    </div>
                </div>
            </div>
            @endif
        </div>

        <div class="col-lg-4 col-sm-4">
            <div class="card">
                <div class="card-header text-center text-primary font-weight-bold">
                    {{__('dashboard.course.instructor')}}
                </div>
                <div class="card-body">
                    <div class="text-center">
                        <img src="{{$course->instructor->getProfileImagePath()}}" style="width: 80px;"  class="rounded-circle" alt="">
                        <h5 class="mt-3 mb-1">{{$course->instructor->getName()}}</h5>
                        <p class="m-0">{{$course->instructor->getDesignation()}}</p>
                    </div>
                    <div class="mt-3">
                        <h4>{{__('dashboard.course.about')}}</h4>
                        <p class="text-muted">{!! nl2br(e($course->instructor->getDescription())) !!}</p>
                    </div>
                </div>
            </div>

            @if(auth()->user()->isStudent() && $feedback)
            <div class="card">
                <div class="card-header text-center text-primary font-weight-bold">
                    {{__('dashboard.course.your_feedback')}}
                </div>
                <div class="card-body">
                    <div class="mt-3">
                        @include('common.feedback_stars')
                        
                        <p class="text-muted">{{$feedback->description}}</p>
                        <div class="d-flex justify-content-center">
                            <a href="{{route('student.course.feedback.edit', [$course, $feedback])}}" class="btn btn-warning">{{__('dashboard.course.edit_feedback')}}</a>
                        </div>
                    </div>
                </div>
            </div>
            @endif

        </div>
    </div>
@endsection
