@extends('dashboard.layouts.main')

@section('page_title', 'Course edit')

@section('sidebar')
    @include('dashboard.layouts.sidebar')
@endsection

@section('content')
    <div class="col-lg-12">
        @include('dashboard.components.validation-errors')

        <div class="card">
            <div class="card-header">
                <h4 class="text-center">{{__('dashboard.course.update_course')}}</h4>
            </div>
            <div class="card-body">
                <div class="form-validation">
                    <form class="form-valide" action="{{route('courses.update', $course)}}" method="post" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        
                        <div class="form-group row">
                            <label class="col-lg-4 col-form-label">Title <span class="text-danger">*</span>
                            </label>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" name="title" value="{{old('title', $course->title)}}" placeholder="Enter a Title.." required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-4 col-form-label">Type <span class="text-danger">*</span>
                            </label>
                            <div class="col-lg-6">
                                <select class="form-control" name="type">
                                    @foreach(\App\Models\Course::TYPES as $type)
                                    <option {{ (old('type', $course->type) === $type) ? 'selected' : '' }} value="{{$type}}">{{ucfirst($type)}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-4 col-form-label">Status <span class="text-danger">*</span>
                            </label>
                            <div class="col-lg-6">
                                <select class="form-control" name="status">
                                    @foreach(\App\Models\Course::STATUSES as $status)
                                        <option {{ (old('status', $course->status) === $status) ? 'selected' : '' }} value="{{$status}}">{{$status}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-4 col-form-label">Course Fee <span class="text-danger">*</span>
                            </label>
                            <div class="col-lg-6">
                                <input type="number" class="form-control" name="fee" value="{{old('fee', $course->fee)}}" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-4 col-form-label">Discount
                            </label>
                            <div class="col-lg-6">
                                <input type="number" class="form-control" name="discount" value="{{old('discount', $course->discount)}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-4 col-form-label">Estimated completion time (in minute)
                            </label>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" name="estimated_completion_time" value="{{old('estimated_completion_time', $course->estimated_completion_time)}}" placeholder="Estimated Completion time">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-4 col-form-label" for="public_description">Public Description
                            </label>
                            <div class="col-lg-6">
                                <textarea class="form-control" id="public_description" name="public_description" rows="5" placeholder="Public Description">{{old('public_description', $course->public_description)}}</textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-4 col-form-label" for="bn_public_description">Bangla Public Description
                            </label>
                            <div class="col-lg-6">
                                <textarea class="form-control" id="bn_public_description" name="bn_public_description" rows="5" placeholder="Bangla Public Description">{{old('bn_public_description', $course->bn_public_description)}}</textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-4 col-form-label" for="private_description">Private Description
                            </label>
                            <div class="col-lg-6">
                                <textarea class="form-control" id="private_description" name="private_description" rows="5" placeholder="Private Description">{{old('private_description', $course->private_description)}}</textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-4 col-form-label" for="bn_private_description">Bangla Private Description
                            </label>
                            <div class="col-lg-6">
                                <textarea class="form-control" id="bn_private_description" name="bn_private_description" rows="5" placeholder="Bangla Private Description">{{old('bn_private_description', $course->bn_private_description)}}</textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-4 col-form-label" for="image">Banner Image
                            </label>
                            <div class="col-lg-6">
                                <div class="mb-3">
                                    <img src="{{asset($course->getBannerImagePath())}}" alt="" style="max-width: 100%;">
                                </div>
                                <input type="file" id="image" name="image" class="form-control-file">
                            </div>
                        </div>

                        <div class="d-flex justify-content-center">
                            <button type="submit" class="btn btn-success">{{__('dashboard.buttons.submit')}}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
