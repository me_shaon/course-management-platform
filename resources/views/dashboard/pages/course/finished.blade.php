@extends('dashboard.layouts.main')

@section('page_title', 'Finished Courses')

@section('sidebar')
    @include('dashboard.layouts.sidebar')
@endsection

@section('content')
    @if($finishedCourses->isEmpty())
        @include('dashboard.components.empty-finished-courses')
    @else
        @include('dashboard.components.finished-courses-component')
    @endif
@endsection
