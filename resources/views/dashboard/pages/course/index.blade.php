@extends('dashboard.layouts.main')

@section('page_title', 'Course List')

@section('sidebar')
    @include('dashboard.layouts.sidebar')
@endsection

@section('content')
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title pull-left">{{__('dashboard.course.course_list')}}</h4>
                <a class="pull-right btn btn-info" href="{{ route('courses.create') }}">Create New Course</a>
                <div class="table-responsive mt-5">
                    <table class="table table-bordered table-striped verticle-middle">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Course Title</th>
                            <th scope="col">Instructor</th>
                            <th scope="col">Course Type</th>
                            <th scope="col">Course Status</th>
                            <th scope="col">Course Fee</th>
                            <th scope="col">Discount</th>
                            <th scope="col">Total Lessons</th>
                            <th scope="col">Total Enrollment</th>
                            <th scope="col">Manage Chapters</th>
                            <th scope="col">Edit</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($courses as $course)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td><a class="text-primary" href="{{route('courses.show', $course)}}" >{{$course->title}}</a></td>
                                <td>{{$course->instructor->name}}</td>
                                <td>{{$course->type}}</td>
                                <td class="text-{{$course->getStatusColor()}}">{{$course->status}}</td>
                                <td>{{$course->getFee()}}</td>
                                <td>{{$course->discount}}</td>
                                <td>{{$course->number_of_lessons}}</td>
                                <td>{{$course->enrollments->count()}}</td>
                                <td class="text-center">
                                    <span><a href="{{ route('chapters.index', $course)}}" data-toggle="tooltip" data-placement="top" title="" data-original-title="Manage Chapters"><i class="fa fa-book color-muted m-r-5"></i></a></span>
                                </td>
                                <td class="text-center">
                                    <span><a href="{{ route('courses.edit', $course)}}" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"><i class="fa fa-pencil color-muted m-r-5"></i></a></span>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
