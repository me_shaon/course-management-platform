@extends('dashboard.layouts.main')

@section('page_title', 'Dashboard')

@section('sidebar')
    @include('dashboard.layouts.sidebar')
@endsection

@section('content')
    @if($onGoingCourses->isEmpty() && $availableCourses->isEmpty() && $finishedCourses->isEmpty())
        @include('dashboard.components.empty-dashboard')
    @else
        @include('dashboard.components.ongoing-courses-component')
        @include('dashboard.components.available-courses-component')
        @include('dashboard.components.finished-courses-component')
    @endif
@endsection
