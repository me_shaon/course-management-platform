@extends('dashboard.layouts.main')

@section('page_title', 'Feedback List')

@section('sidebar')
    @include('dashboard.layouts.sidebar')
@endsection

@section('content')
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">{{__('dashboard.course.feedback_list')}}</h4>
                <div class="table-responsive">
                    <table class="table header-border">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>User</th>
                            <th>Course</th>
                            <th>Rating</th>
                            <th>Description</th>
                            <th>Status</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($feedbacks as $feedback)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$feedback->user->name}}</td>
                                <td class="text-primary">{{$feedback->course->title}}</td>
                                <td>{{$feedback->rating}}</td>
                                <td>{{$feedback->description}}</td>
                                <td>{{$feedback->is_approved ? 'Approved' : 'Pending'}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{$feedbacks->links()}}
                </div>
            </div>
        </div>
    </div>
@endsection
