@extends('dashboard.layouts.main')

@section('page_title', 'Course Feedback')

@section('sidebar')
    @include('dashboard.layouts.sidebar')
@endsection

@section('content')
    <div class="row">
        <div class="col-8 offset-2">

            @include('dashboard.layouts.common.errors')
            <div class="card">
                <div class="card-body">
                    <div class="card-title text-center">
                        <h5 class="text-success">
                            {{__('dashboard.course.give_feedback')}}
                        </h5>
                        <h3 class="text-danger">{{$course->title}}</h3>
                        <hr>
                    </div>

                    <form class="mt-4" action="{{route('student.course.feedback.store', $course)}}" method="POST">
                        @csrf

                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label text-right">Rating <span class="text-danger">*</span>
                            </label>
                            <div class="col-lg-8">
                                <select class="form-control" name="rating">
                                    @foreach(\App\Models\Feedback::RATINGS as $rating)
                                    <option {{ (old('rating') === $rating) ? 'selected' : '' }} value="{{$rating}}">{{\App\Models\Feedback::RATING_LABELS[$rating]}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label text-right">Feedback <span class="text-danger">*</span>
                            </label>
                            <div class="col-lg-8">
                                <textarea class="form-control" id="description" name="description" rows="5" placeholder="Please briefly write what you liked or disliked ...">{{old('description')}}</textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="d-flex justify-content-center">
                                <button type="submit" class="btn btn-success">{{__('dashboard.buttons.submit')}}</button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
@endsection
