@extends('dashboard.layouts.main')

@section('page_title', 'Profile edit')

@section('sidebar')
    @include('dashboard.layouts.sidebar')
@endsection

@section('content')
    <div class="col-lg-12">
        @include('dashboard.components.validation-errors')

        <div class="card">
            <div class="card-header">
                <h4 class="text-center">{{__('dashboard.profile.update')}}</h4>
            </div>
            <div class="card-body">
                <div class="form-validation">
                    <form class="form-valide" action="{{route('profile.update')}}" method="post" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        
                        <div class="form-group row">
                            <label class="col-lg-4 col-form-label">Name <span class="text-danger">*</span>
                            </label>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" name="name" value="{{old('name', $user->name)}}" placeholder="Enter a Name.." required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-4 col-form-label">Name (in Bangla)
                            </label>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" name="bn_name" value="{{old('bn_name', $user->bn_name)}}" placeholder="Enter the Name in Bangla..">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-4 col-form-label">Designation <span class="text-danger">*</span>
                            </label>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" name="designation" value="{{old('designation', $user->designation)}}" placeholder="Enter a Designation..." required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-4 col-form-label">Designation (in Bangla)
                            </label>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" name="bn_designation" value="{{old('bn_designation', $user->bn_designation)}}" placeholder="Enter the Designation in Bangla...">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-4 col-form-label" for="description">Description
                            </label>
                            <div class="col-lg-6">
                                <textarea class="form-control" id="description" name="description" rows="5" placeholder="Description">{{old('description', $user->description)}}</textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-4 col-form-label" for="description">Description (in Bangla)
                            </label>
                            <div class="col-lg-6">
                                <textarea class="form-control" id="bn_description" name="bn_description" rows="5" placeholder="Description (in Bangla)">{{old('bn_description', $user->bn_description)}}</textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-4 col-form-label">Github Link
                            </label>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" name="github_url" value="{{old('github_url', $user->github_url)}}" placeholder="Enter your Github URL..." >
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-4 col-form-label">LinkedIn Link
                            </label>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" name="linkedin_url" value="{{old('linkedin_url', $user->linkedin_url)}}" placeholder="Enter your LinkedIn URL..." >
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-4 col-form-label">Facebook Link
                            </label>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" name="facebook_url" value="{{old('facebook_url', $user->facebook_url)}}" placeholder="Enter your Facebook URL..." >
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-4 col-form-label">Website Link
                            </label>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" name="website_url" value="{{old('website_url', $user->website_url)}}" placeholder="Enter your Website URL..." >
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-lg-4 col-form-label" for="image">Image
                            </label>
                            <div class="col-lg-6">
                                <div class="mb-3">
                                    <img src="{{$user->getProfileImagePath()}}" alt="" style="max-width: 100%;">
                                </div>
                                <input type="file" id="image" name="image" class="form-control-file">
                            </div>
                        </div>

                        <div class="d-flex justify-content-center">
                            <button type="submit" class="btn btn-success">{{__('dashboard.buttons.submit')}}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
