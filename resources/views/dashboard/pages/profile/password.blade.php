@extends('dashboard.layouts.main')

@section('page_title', 'Password Update')

@section('sidebar')
    @include('dashboard.layouts.sidebar')
@endsection

@section('content')
    <div class="col-lg-12">
        @include('dashboard.components.validation-errors')

        <div class="card">
            <div class="card-header">
                <h4 class="text-center">{{__('dashboard.profile.password_update')}}</h4>
            </div>
            <div class="card-body">
                <div class="form-validation">
                    <form class="form-valide" action="{{route('profile.password.update')}}" method="post">
                        @csrf
                        @method('PATCH')
                        
                        <div class="form-group row">
                            <label class="col-lg-4 col-form-label">Password <span class="text-danger">*</span>
                            </label>
                            <div class="col-lg-6">
                                <input type="password" class="form-control" name="password" value="" placeholder="Enter a Password.." required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-4 col-form-label">Confirm Password <span class="text-danger">*</span>
                            </label>
                            <div class="col-lg-6">
                                <input type="password" class="form-control" name="password_confirmation" value="" placeholder="Re-enter the Password.." required>
                            </div>
                        </div>
                    
                        <div class="d-flex justify-content-center">
                            <button type="submit" class="btn btn-success">{{__('dashboard.buttons.submit')}}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
