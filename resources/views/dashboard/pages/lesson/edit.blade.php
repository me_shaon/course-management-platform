@extends('dashboard.layouts.main')

@section('page_title', 'Lesson create')

@section('sidebar')
    @include('dashboard.layouts.sidebar')
@endsection

@section('content')
    <div class="col-lg-12">
        @include('dashboard.components.validation-errors')

        <div class="card">
            <div class="card-header">
                <h4 class="text-center">{{__('dashboard.course.edit_lesson')}}</h4>
            </div>
            <div class="card-body">
                <div class="form-validation">
                    <form class="form-valide" action="{{route('lessons.update', [$course, $chapter, $lesson])}}" method="post">
                        @csrf
                        @method('PUT')
                        
                        <div class="form-group row">
                            <label class="col-lg-4 col-form-label">Title <span class="text-danger">*</span>
                            </label>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" name="title" value="{{old('title', $lesson->title)}}" placeholder="Enter a Title.." required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-4 col-form-label" for="description">Description
                            </label>
                            <div class="col-lg-6">
                                <textarea class="form-control" id="description" name="description" rows="5" placeholder="Description">{{old('description', $lesson->description)}}</textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-4 col-form-label">Link <span class="text-danger">*</span>
                            </label>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" name="link" value="{{old('link', $lesson->link)}}" placeholder="Enter a Video Link.." required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-4 col-form-label">Is Free<span class="text-danger">*</span>
                            </label>
                            <div class="col-lg-6">
                                <div class="form-check mb-3">
                                    <input type="checkbox" name="is_free" class="form-check-input" value="1" {{ $lesson->is_free ? 'checked' : ''}}>
                                </div>
                            </div>
                        </div>

                        <div class="d-flex justify-content-center">
                            <button type="submit" class="btn btn-success">{{__('dashboard.buttons.submit')}}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
