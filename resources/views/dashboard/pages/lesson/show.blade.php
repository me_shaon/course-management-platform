@extends('dashboard.layouts.main')

@section('page_title', 'Course - ' . $course->title)

@section('sidebar')
    @include('dashboard.layouts.course-contents-sidebar')
@endsection

@section('breadcrumb')
    @include('dashboard.components.course-contents-breadcrumb')
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">{{$lesson->title}}</h4>
                    <div class="plyr__video-embed" id="player">
                        <iframe
                            src="{{getYoutubeVideoLink($lesson->link)}}"
                            allowfullscreen
                            allowtransparency
                            allow="autoplay"
                        ></iframe>
                    </div>
                </div>

                @can('viewAllLesson', [$course, $enrollment])
                    <div class="card-footer">
                        @if($lesson->prev)
                            <a class="btn btn-info pull-left"
                               data-toggle="tooltip"
                               data-placement="top"
                               title=""
                               data-original-title="{{$lesson->prev->title}}"
                               href="{{route('student.course.lesson.show', [$course, $lesson->prev->order])}}"
                            >
                                {{__('dashboard.course.previous_lesson')}}
                            </a>
                        @endif

                        @if($lesson->next)
                            <a class="btn btn-info pull-right"
                               data-toggle="tooltip"
                               data-placement="top"
                               title=""
                               data-original-title="{{$lesson->next->title}}"
                               href="{{route('student.course.lesson.show', [$course, $lesson->next->order])}}"
                            >
                            {{__('dashboard.course.next_lesson')}}
                            </a>
                        @else
                            <form action="{{route('student.course.finish', $course)}}" method="post">
                                @csrf
                                @method('PATCH')

                                <button 
                                    type="submit"
                                    class="btn btn-success pull-right"
                                    data-toggle="tooltip"
                                    data-placement="top"
                                    title=""
                                    data-original-title="{{__('dashboard.course.mark_finish_text')}}"
                                >
                                    {{__('dashboard.course.mark_finish_button')}}
                                </button>
                            </form>
                        @endif
                    </div>
                @endcan
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function () {
            var player = new Plyr('#player');
        });
    </script>
@endsection
