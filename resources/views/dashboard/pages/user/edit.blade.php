@extends('dashboard.layouts.main')

@section('page_title', 'User Edit')

@section('sidebar')
    @include('dashboard.layouts.sidebar')
@endsection

@section('content')
    <div class="col-lg-12">
        @include('dashboard.components.validation-errors')

        <div class="card">
            <div class="card-header">
                <h4 class="text-center">{{__('dashboard.user.update')}}</h4>
            </div>
            <div class="card-body">
                <div class="form-validation">
                    <form class="form-valide" action="{{route('users.update', $user)}}" method="post">
                        @csrf
                        @method('PUT')
                        
                        <div class="form-group row">
                            <label class="col-lg-4 col-form-label">Name <span class="text-danger">*</span>
                            </label>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" name="name" value="{{old('name', $user->name)}}" placeholder="Enter a Name.." required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-4 col-form-label">Email <span class="text-danger">*</span>
                            </label>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" name="email" value="{{old('email', $user->email)}}" placeholder="Enter an Email.." required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-4 col-form-label">Role <span class="text-danger">*</span>
                            </label>
                            <div class="col-lg-6">
                                <select class="form-control" name="role">
                                    <option {{ (old('role', $user->role) === App\Models\User::ROLE_INSTRUCTOR) ? 'selected' : '' }} value="{{App\Models\User::ROLE_INSTRUCTOR}}">{{ucfirst(App\Models\User::ROLE_INSTRUCTOR)}}</option>
                                    <option {{ (old('role', $user->role) === App\Models\User::ROLE_ADMIN) ? 'selected' : '' }} value="{{App\Models\User::ROLE_ADMIN}}">{{ucfirst(App\Models\User::ROLE_ADMIN)}}</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-4 col-form-label">Password
                            </label>
                            <div class="col-lg-6">
                                <input type="password" class="form-control" name="password" value="{{old('password')}}" placeholder="Enter updated Password..">
                            </div>
                        </div>

                        <div class="d-flex justify-content-center">
                            <button type="submit" class="btn btn-success">{{__('dashboard.buttons.submit')}}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
