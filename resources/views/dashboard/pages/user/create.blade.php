@extends('dashboard.layouts.main')

@section('page_title', 'User create')

@section('sidebar')
    @include('dashboard.layouts.sidebar')
@endsection

@section('content')
    <div class="col-lg-12">
        @include('dashboard.components.validation-errors')

        <div class="card">
            <div class="card-header">
                <h4 class="text-center">{{__('dashboard.user.create')}}</h4>
            </div>
            <div class="card-body">
                <div class="form-validation">
                    <form class="form-valide" action="{{route('users.store')}}" method="post">
                        @csrf
                        
                        <div class="form-group row">
                            <label class="col-lg-4 col-form-label">Name <span class="text-danger">*</span>
                            </label>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" name="name" value="{{old('name')}}" placeholder="Enter a Name.." required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-4 col-form-label">Email <span class="text-danger">*</span>
                            </label>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" name="email" value="{{old('email')}}" placeholder="Enter an Email.." required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-4 col-form-label">Role <span class="text-danger">*</span>
                            </label>
                            <div class="col-lg-6">
                                <select class="form-control" name="role">
                                    <option {{ (old('role') === App\Models\User::ROLE_INSTRUCTOR) ? 'selected' : '' }} value="{{App\Models\User::ROLE_INSTRUCTOR}}">{{ucfirst(App\Models\User::ROLE_INSTRUCTOR)}}</option>
                                    <option {{ (old('role') === App\Models\User::ROLE_ADMIN) ? 'selected' : '' }} value="{{App\Models\User::ROLE_ADMIN}}">{{ucfirst(App\Models\User::ROLE_ADMIN)}}</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-4 col-form-label">Password <span class="text-danger">*</span>
                            </label>
                            <div class="col-lg-6">
                                <input type="password" class="form-control" name="password" value="{{old('password')}}" placeholder="Enter a Password.." required>
                            </div>
                        </div>

                        <div class="d-flex justify-content-center">
                            <button type="submit" class="btn btn-success">{{__('dashboard.buttons.submit')}}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
