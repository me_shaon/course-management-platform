@extends('dashboard.layouts.main')

@section('page_title', 'Chapters')

@section('sidebar')
    @include('dashboard.layouts.sidebar')
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            @include('dashboard.components.validation-errors')

            <div class="card">
                <div class="card-body">
                    <h4 class="card-title text-center text-primary">{{__('dashboard.course.add_new_chapter')}}</h4>
                    <div class="form-validation">
                        <form class="form-valide" action="{{route('chapters.store', $course)}}" method="post">
                            @csrf
                            
                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label text-right">Title <span class="text-danger">*</span>
                                </label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" name="title" value="{{old('title')}}" placeholder="Enter a Title.." required>
                                </div>
                            </div>

                            <input type="hidden" name="course_id" value="{{$course->id}}">
                            <input type="hidden" name="order" value="{{$chapters->count() + 1}}">

                            <div class="form-group row text-center">
                                <div class="col-lg-12 ml-auto">
                                    <button type="submit" class="btn btn-success">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @foreach ($chapters as $chapter)
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header bg-white">
                        <div class="accordion">
                            <div class="card">
                                <div class="card-header">
                                    <h5 class="mb-0" data-toggle="collapse" data-target="#chapter{{$chapter->id}}" aria-expanded="true" aria-controls="chapter{{$chapter->id}}"><i class="fa fa-edit pull-right" aria-hidden="true"></i> {{$chapter->title}}</h5>
                                </div>
                                <div id="chapter{{$chapter->id}}" class="collapse">
                                    <div class="card-body">
                                        <div class="form-validation">
                                            <form class="form-valide" action="{{route('chapters.update', [$course, $chapter])}}" method="post">
                                                @csrf
                                                @method('PUT')
                                                
                                                <div class="form-group row">
                                                    <label class="col-lg-2 col-form-label text-right">Title <span class="text-danger">*</span>
                                                    </label>
                                                    <div class="col-lg-10">
                                                        <input type="text" class="form-control" name="title" value="{{old('title', $chapter->title)}}" placeholder="Enter a Title.." required>
                                                    </div>
                                                </div>
                    
                                                <input type="hidden" name="course_id" value="{{$course->id}}">
                                                <input type="hidden" name="order" value="{{$chapter->order}}">
                    
                                                <div class="form-group row text-center">
                                                    <div class="col-lg-12 ml-auto">
                                                        <button type="submit" class="btn btn-success">Update</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="basic-list-group">
                            <ul class="list-group">
                                @foreach ($chapter->lessons as $lesson)
                                <li class="list-group-item d-flex justify-content-between align-items-center">{{$lesson->title}} 
                                    <span class="">
                                        <a href="{{route('lessons.edit', [$course, $chapter, $lesson])}}" class="pull-right"><i class="fa fa-edit"></i></a>
                                    </span>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="form-group row text-center">
                            <div class="col-lg-12 ml-auto">
                                <a href="{{route('lessons.create', [$course, $chapter])}}" class="btn btn-primary">Add New Lesson</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@endsection
