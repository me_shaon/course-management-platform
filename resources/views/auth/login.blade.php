@extends('website.layouts.main')

@section('page_title', 'Login')

@section('content')

<div class="slider_area ">
    <div class="single_slider d-flex align-items-center justify-content-center slider_bg_1">
        <div class="container">
            <div class="row align-items-center justify-content-center">
                <div class="col-xl-6 col-md-6">
                    <div class="illastrator_png">
                        <img src="{{asset('assets/website/img/banner/edu_ilastration.png')}}" alt="">
                    </div>
                </div>
                <div class="col-xl-6 col-md-6">
                    <div class="slider_info">
                        @if (request()->get('as') == \App\Models\User::ROLE_INSTRUCTOR)
                            @include('website.components.instructor_login_form')
                        @else
                            @include('website.components.student_login_form')
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
