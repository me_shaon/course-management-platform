<?php

return [
    'header' => [
        'go_to_website' => 'Go to Website',
        'update_password' => 'Update Password',
        'update_profile' => 'Update Profile',
        'log_out' => 'Log Out'
    ],
    'sidebar' => [
        'dashboard' => 'Dashboard',
        'users' => 'Users',
        'user_list' => 'User List',
        'create_new_user' => 'Create New User',
        'my_courses' => 'My Courses',
        'ongoing_courses' => 'Ongoing Courses',
        'finished_courses' => 'Finished Courses',
        'courses' => 'Courses',
        'course_list' => 'Course List',
        'create_new_course' => 'Create New Course',
        'coupons' => 'Coupons',
        'coupon_list' => 'Coupon List',
        'create_new_coupon' => 'Create new Coupon',
        'enrollments' => 'Enrollments',
        'enrollment_list' => 'Enrollment List',
        'pending_enrollments' => 'Pending Enrollments',
        'feedbacks' => 'Feedbacks',
        'feedback_list' => 'Feedback List',
        'pending_feedbacks' => 'Pending Feedbacks'
    ],
    'buttons' => [
        'submit' => 'Submit',
    ],
    'profile' => [
        'update' => 'Profile Update',
        'password_update' => 'Password Update'
    ],
    'user' => [
        'create' => 'Create User',
        'update' => 'Updtae User',
        'list' => 'User List'
    ],
    'course' => [
        'available_courses' => 'Available Courses',
        'enroll' => 'Enroll',
        'chapters' => 'Chapters',
        'go_to_class' => 'Go to class',
        'your_ongoing_courses' => 'Your ongoing courses',
        'your_finished_courses' => 'Your finished courses',
        'no_ongoing' => 'You have nothing going on.',
        'no_finished' => 'You have no finished courses.',
        'lets_enroll' => "Let's enroll to some courses.",
        'lets_revisit' => "Let's re-visit the course.",
        'buy_to_view' => 'You need to buy the course to view this content',
        'empty_dashboard' => 'We are working hard to create some awesome courses. <br> They will be published soon.',
        'add_new_chapter' => 'Add new Chapter',
        'create_coupon' => 'Create Coupon',
        'update_coupon' => 'Update Coupon',
        'coupon_list' => 'Coupon List',
        'create_course' => 'Create Course',
        'update_course' => 'Update Course',
        'course_list' => 'Course List',
        'welcome' => 'Welcome to the course',
        'number_of_lessons' => 'Number of Lessons',
        'completion_time' => 'Estimated Completion time',
        'lets_explore' => "Let's explore this awesome course",
        'description' => 'Course Description',
        'private_note' => 'Special Note from Instructor',
        'instructor' => 'Instructor',
        'about' => 'About',
        'your_feedback' => 'Your feedback',
        'edit_feedback' => 'Edit Feedback',
        'enrollment_list' => 'Enrollment List',
        'pending_enrollments_for_approval' => 'Pending Enrollments for Approval',
        'give_feedback' => 'Please give your feedback about the course',
        'update_feedback' => 'Want to update your feedback?',
        'feedback_list' => 'Feedback List',
        'create_lesson' => 'Create Lesson',
        'edit_lesson' => 'Edit Lesson',
        'previous_lesson' => 'Previous Lesson',
        'next_lesson' => 'Next Lesson',
        'mark_finish_button' => 'Mark Finished',
        'mark_finish_text' => 'Mark this course as Finished',
    ],
    'purchase' => [
        'apply_coupon_button_text' => 'Apply Coupon Code',
        'thanks_for_interest' => 'Thanks for your interest to purchase the course',
        'notice_transaction_limit' => "We are currently receiving the payment to a Personal Bkash number. If you are unable to make the transaction due to the 'Daily Transaction Limit' of the given Bkash number, We're extremely sorry for that. In that case, please try to purchase the course tomorrow.",
        'transaction_instruction' => 'Please send money of amount: <span class="font-weight-bold">:amount</span><br/>to the Bkash <span class="font-weight-bold">Personal number: :bkash_number</span><br/>and fill up the following form.',
        'no_transaction_fee' => '<span class="font-weight-bold">Congratulations! The course is completely free for you.</span><br> Enjoy the course by adding your phone number and the text "FREE" in the "Transaction ID" field and submit.',
        'pending_for_approval' => "Your purchase request is pending for approval. We'll review it shortly.",
        'you_need_to_purchase' => 'You need to purchase the course to view all of the amazing lessons',
        'buy_this_course' => 'Buy this course',
        'only' => 'Only',
        'success' => [
            'title' => 'Thanks a lot!',
            'body' => 'We have received your transaction information. We will verify it shortly and give you full access to the course.',
            'button_text' => "Let's get back to the Course Home"
        ]
    ],
    'errors' => [
        'not_authorized_text' => 'Sorry, you are not authorized to do this action.',
        'go_home_button' => 'Please go home',
        'not_found_text' => 'Looks like everything is dark here.',
        'server_error_text' => 'Ooops! Looks like something went very very wrong.'
    ]
];
