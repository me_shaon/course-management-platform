<?php

return [
    'hero_title' => 'Advance your career by Megaminds Learning',
    'enroll_button_text' => 'Enroll in this course',
    'our_courses' => 'Our Courses',
    'home' => 'Home',
    'contact' => 'Contact',
    'browse_course_button_text' => 'Browse Our Courses',
    'my_dashboard' => 'My Dashboard',
    'log_in' => 'Login',
    'log_out' => 'Log Out',
    'as_student' => 'As Student',
    'as_instructor' => 'As Instructor',
    'all_courses' => 'All Courses',
    'instructor' => 'Instructor',
    'go_to_dashboard' => 'Go to Course Dashboard',
    'course_outline' => 'Course Outline',
    'sign_in_text' => "Let's Sign in",
    'forgot_password_text' => "Oh snap! Can't remember your password?",
    'not_available' => 'The course will be available soon',
    'feedback_title' => 'Feedback about the course',
];
