<?php

return [
    'header' => [
        'go_to_website' => 'ওয়েবসাইটে চলুন',
        'update_password' => 'পাসওয়ার্ড আপডেট',
        'update_profile' => 'প্রোফাইল আপডেট',
        'log_out' => 'লগ আউট'
    ],
    'sidebar' => [
        'dashboard' => 'ড্যাশবোর্ড',
        'users' => 'ইউজার',
        'user_list' => 'ইউজার লিস্ট',
        'create_new_user' => 'নতুন ইউজার তৈরি',
        'my_courses' => 'আমার কোর্সসমূহ',
        'ongoing_courses' => 'চলমান কোর্সসমূহ',
        'finished_courses' => 'সমাপ্ত কোর্সসমূহ',
        'courses' => 'কোর্স',
        'course_list' => 'কোর্স লিস্ট',
        'create_new_course' => 'নতুন কোর্স তৈরি',
        'coupons' => 'কুপন',
        'coupon_list' => 'কুপন লিস্ট',
        'create_new_coupon' => 'নতুন কুপন তৈরি ',
        'enrollments' => 'এনরোলমেন্ট',
        'enrollment_list' => 'এনরোলমেন্ট লিস্ট',
        'pending_enrollments' => 'পেন্ডিং এনরোলমেন্টসমূহ',
        'feedbacks' => 'ফিডব্যাক',
        'feedback_list' => 'ফিডব্যাক লিস্ট',
        'pending_feedbacks' => 'পেন্ডিং ফিডব্যাকসমূহ'
    ],
    'buttons' => [
        'submit' => 'সাবমিট',
    ],
    'profile' => [
        'update' => 'প্রোফাইল আপডেট',
        'password_update' => 'পাসওয়ার্ড আপডেট'
    ],
    'user' => [
        'create' => 'ইউজার তৈরি',
        'update' => 'ইউজার আপডেট',
        'list' => 'ইউজার লিস্ট'
    ],
    'course' => [
        'available_courses' => 'কোর্সসমূহ',
        'enroll' => 'এনরোল',
        'chapters' => 'অধ্যায়সমূহ',
        'go_to_class' => 'ক্লাসে চলুন',
        'your_ongoing_courses' => 'আপনার চলমান কোর্সসমূহ',
        'your_finished_courses' => 'আপনার সমাপ্ত কোর্সসমূহ',
        'no_ongoing' => 'আপনার কোনো কোর্স এখন চলমান নেই।',
        'no_finished' => 'আপনার কোনো কোর্স এখনো সমাপ্ত হয়নি।',
        'lets_enroll' => "চলুন কিছু কোর্স শুরু করা যাক",
        'lets_revisit' => "কোর্সটি পুনরায় দেখে আসুন",
        'buy_to_view' => 'এই কন্টেন্টটি দেখতে হলে আপনাকে কোর্সটি কিনতে হবে',
        'empty_dashboard' => 'আমরা খুব দ্রুতই কিছু কোর্স পাবলিশ করার জন্য যথাসাধ্য চেষ্টা করে যাচ্ছি।',
        'add_new_chapter' => 'নতুন অধ্যায় তৈরি',
        'create_coupon' => 'কুপন তৈরি',
        'update_coupon' => 'কুপন আপডেট',
        'coupon_list' => 'কুপন লিস্ট',
        'create_course' => 'কোর্স তৈরি',
        'update_course' => 'কোর্স আপডেট',
        'course_list' => 'কোর্স লিস্ট',
        'welcome' => 'আপনাকে স্বাগতম ',
        'number_of_lessons' => 'মোট পাঠ',
        'completion_time' => 'সম্ভাব্য সমাপ্তির সময়',
        'lets_explore' => "চলুন কোর্সটি শুরু করা যাক",
        'description' => 'কোর্সের বিস্তারিত',
        'private_note' => 'প্রশিক্ষকের পক্ষ থেকে স্পেশাল নোট',
        'instructor' => 'প্রশিক্ষক',
        'about' => 'বিবরণ',
        'your_feedback' => 'আপনার মতামত',
        'edit_feedback' => 'মতামত পরিবর্তন ',
        'enrollment_list' => 'এনরোলমেন্ট লিস্ট',
        'pending_enrollments_for_approval' => 'এপ্রোভালের জন্য পেন্ডিং এনরোলমেন্টসমূহ',
        'give_feedback' => 'কোর্সটি সম্পর্কে আপনার মতামত জানান',
        'update_feedback' => 'আপনার মতামত পরিবর্তন করতে চান?',
        'feedback_list' => 'ফিডব্যাক লিস্ট',
        'create_lesson' => 'পাঠ তৈরি',
        'edit_lesson' => 'পাঠ এডিট',
        'previous_lesson' => 'পূর্ববর্তী পাঠ',
        'next_lesson' => 'পরবর্তী পাঠ',
        'mark_finish_button' => 'সমাপ্ত করুন',
        'mark_finish_text' => 'কোর্সটিকে সমাপ্ত হিসেবে মার্ক করুন',
    ],
    'purchase' => [
        'apply_coupon_button_text' => 'কুপন কোড এপ্লাই করুন',
        'thanks_for_interest' => 'ধন্যবাদ কোর্সটি কিনতে আগ্রহী হওয়ার জন্য',
        'notice_transaction_limit' => "আমরা আপাতত একটি বিকাশ পার্সোনাল নাম্বারে পেমেন্ট গ্রহণ করছি। আপনি ট্রানজেকশন করতে গিয়ে যদি দেখেন যে নাম্বারটির 'ডেইলি লিমিট' ওভার হয়ে গিয়েছে, তাহলে দয়া করে আগামীদিন আবার ট্রাই করুন ট্রানজেকশনটি করার। এই অনাকাঙ্ক্ষিত সমস্যার জন্য আমরা আন্তরিকভাবে দুঃখিত।",
        'transaction_instruction' => 'দয়া করে <span class="font-weight-bold">:amount</span> বিকাশ <span class="font-weight-bold">(পার্সোনাল নাম্বার): :bkash_number</span> এ পাঠান <br/>এবং নীচের ফর্মটি পূরণ করুন।',
        'no_transaction_fee' => '<span class="font-weight-bold">অভিবাদন! এই কোর্সটি আপনার জন্য সম্পূর্ণ ফ্রি।</span><br> দয়া করে নীচের ফর্মে আপনার ফোন নাম্বার এবং "Transaction ID" ফিল্ডে "FREE" লিখে সাবমিট করে উপভোগ করুন কোর্সটি সম্পূর্ণ ফ্রিতে।',
        'pending_for_approval' => "কোর্সটি ক্রয় করার জন্য আপনার আবেদনটি অপেক্ষমাণ আছে। আমরা শীঘ্রই এটি চেক করবো।",
        'you_need_to_purchase' => 'এই চমৎকার কোর্সের সবগুলো ভিডিও দেখতে চাইলে আপনাকে কোর্সটি ক্রয় করতে হবে।',
        'buy_this_course' => 'কোর্সটি কিনুন',
        'only' => 'মাত্র',
        'success' => [
            'title' => 'অসংখ্য ধন্যবাদ!',
            'body' => 'আপনার ট্রানজেকশন ইনফরমেশনটি পেয়েছি। খুব শীঘ্রই আমরা এটি যাচাই করে আপনাকে কোর্সের ফুল এক্সেস দিবো।',
            'button_text' => "আপাতত কোর্সের প্রধান পাতায় ফিরে চলুন"
        ]
    ],
    'errors' => [
        'not_authorized_text' => 'দুঃখিত, আপনার এই কাজের অনুমতি নাই।',
        'go_home_button' => 'বাড়িত যান',
        'not_found_text' => 'এইখানে তো কিচ্ছুই নাই, সব ঘুটঘুইট্টা আন্ধার',
        'server_error_text' => 'আয় হায়! সার্ভার গ্যাঁড়ায় গ্যাছে গা।'
    ]
];
