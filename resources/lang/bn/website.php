<?php

return [
    'hero_title' => 'আপনার ক্যারিয়ারে এগিয়ে যান মেগামাইন্ডস লার্নিংয়ের সাহায্যে',
    'enroll_button_text' => 'এই কোর্সে নিবন্ধন করুন',
    'our_courses' => 'আমাদের কোর্সসমূহ',
    'home' => 'হোম',
    'contact' => 'যোগাযোগ',
    'browse_course_button_text' => 'আমাদের কোর্সসমূহ দেখুন',
    'my_dashboard' => 'আমার ড্যাশবোর্ড',
    'log_in' => 'লগ ইন',
    'log_out' => 'লগ আউট',
    'as_student' => 'শিক্ষার্থী',
    'as_instructor' => 'প্রশিক্ষক',
    'all_courses' => 'সব কোর্স',
    'instructor' => 'প্রশিক্ষক',
    'go_to_dashboard' => 'কোর্সের ড্যাশবোর্ডে চলুন',
    'course_outline' => 'কোর্সের সূচী',
    'sign_in_text' => "সাইন ইন করুন",
    'forgot_password_text' => "এহ হে! পাসওয়ার্ড ভুলে গেছেন?",
    'not_available' => 'কোর্সটি খুব শীঘ্রই অবমুক্ত করা হবে',
    'feedback_title' => 'কোর্সটির ব্যাপারে শিক্ষার্থীদের অভিমত',
];
