<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Course;

class CreateCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('slug');
            $table->string('type')->default(Course::TYPE_FREE);
            $table->string('status')->default(Course::STATUS_UPCOMING);
            $table->unsignedInteger('fee')->default(0);
            $table->unsignedInteger('discount')->default(0);
            $table->unsignedInteger('number_of_lessons')->default(0);
            $table->unsignedInteger('estimated_completion_time')
                ->nullable()
                ->comments('in minute');
            $table->text('public_description')
                ->nullable()
                ->comment('Description to show for everyone');
            $table->text('bn_public_description')
                ->nullable()
                ->comment('Bangla Description to show for everyone');
            $table->text('private_description')
                ->nullable()
                ->comment('Description to show only who purchased the course');
            $table->text('bn_private_description')
                ->nullable()
                ->comment('Description to show only who purchased the course');
            $table->foreignId('instructor_id')
                ->nullable()
                ->constrained('users')
                ->onDelete('set null');
            $table->unsignedInteger('total_enrolled')->default(0);

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('courses', function (Blueprint $table) {
            $table->dropForeign(['instructor_id']);
        });

        Schema::dropIfExists('courses');
    }
}
