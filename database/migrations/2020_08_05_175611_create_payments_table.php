<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Constants\PaymentStatus;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('payable')->default(0)->comment('course fee');
            $table->unsignedInteger('discount')->default(0);
            $table->foreignId('coupon_id')
                ->nullable()
                ->constrained();
            $table->unsignedInteger('paid')->default(0);
            $table->string('method');
            $table->string('type');
            $table->string('category');
            $table->string('transaction_id')->nullable();
            $table->foreignId('enrollment_id')
                ->nullable()
                ->constrained();
            $table->foreignId('reviewed_by')
                ->nullable()
                ->constrained('users');
            $table->text('description')->nullable();
            $table->tinyInteger('status')->default(PaymentStatus::PENDING);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payments', function (Blueprint $table) {
            $table->dropForeign(['coupon_id']);
            $table->dropForeign(['enrollment_id']);
            $table->dropForeign(['reviewed_by']);
        });

        Schema::dropIfExists('payments');
    }
}
