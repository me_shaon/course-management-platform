<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use Illuminate\Support\Str;
use App\Models\User;
use App\Models\Course;

$factory->define(Course::class, function (Faker $faker, array $data) {
    $title = $data['title'] ?? $faker->sentence;
    $instructorIds = User::instructors()->pluck('id')->all();

    return [
        'title' => $title,
        'slug' => Str::slug($title),
        'type' => $data['type'] ?? $faker->randomElement(Course::TYPES),
        'status' => $data['type'] ?? $faker->randomElement(Course::STATUSES),
        'fee' => $data['fee'] ?? $faker->numberBetween(100, 500),
        'discount'=> $data['discount'] ?? $faker->numberBetween(10, 50),
        'number_of_lessons' => $data['number_of_lessons'] ?? $faker->numberBetween(1, 5),
        'estimated_completion_time' => $data['estimated_completion_time'] ?? null,
        'public_description' => $data['public_description'] ?? $faker->text,
        'private_description' => $data['private_description'] ?? $faker->text,
        'instructor_id' => $data['instructor_id'] ?? $faker->randomElement($instructorIds)
    ];
});
