<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Lesson;
use App\Models\Chapter;
use Faker\Generator as Faker;
use Faker\Provider\Youtube as YoutubeProvider;

$factory->define(Lesson::class, function (Faker $faker, array $data) {
    $faker->addProvider(new YoutubeProvider($faker));

    $chapterIds = Chapter::all('id')->pluck('id')->toArray();

    return [
        'title' => $data['title'] ?? $faker->sentence,
        'description' => $data['description'] ?? $faker->text,
        'order' => $data['order'] ?? $faker->numberBetween(1, 5),
        'link' => $data['link'] ?? $faker->youtubeUri(),
        'is_free' => $data['is_free'] ?? $faker->boolean,
        'chapter_id' => $data['chapter_id'] ?? $faker->randomElement($chapterIds)
    ];
});
