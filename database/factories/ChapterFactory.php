<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Chapter;
use App\Models\Course;
use Faker\Generator as Faker;

$factory->define(Chapter::class, function (Faker $faker, array $data) {
    $courseIds = Course::all('id')->pluck('id')->toArray();

    return [
        'title' => $data['title'] ?? $faker->sentence,
        'order' => $data['order'] ?? $faker->numberBetween(1, 5),
        'course_id' => $data['course_id'] ?? $faker->randomElement($courseIds)
    ];
});
