<?php

use Illuminate\Database\Seeder;
use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(User::class)->create([
            'email' => 'admin@megaminds.com',
            'role' => User::ROLE_ADMIN
        ]);

        // factory(User::class)->create([
        //     'email' => 'instructor@megaminds.com',
        //     'role' => User::ROLE_INSTRUCTOR
        // ]);

        // factory(User::class)->create([
        //     'email' => 'student@megaminds.com',
        //     'role' => User::ROLE_STUDENT
        // ]);
    }
}
